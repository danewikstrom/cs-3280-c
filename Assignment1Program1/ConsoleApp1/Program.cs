﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace Assignment1Program2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create variables
            string sFirstNumber;
            string sSecondNumber;
            int iFirstNumber;
            int iSecondNumber;
            int iSum;
            int iDifference;
            int iProduct;
            int iQuotient;
            int iRemainder;


            //Get the numbers
            Console.Write("Enter First Number: ");
            sFirstNumber = Console.ReadLine();
            Console.Write("Enter Second Number: ");
            sSecondNumber = Console.ReadLine();

            //Convert the numbers
            Int32.TryParse(sFirstNumber, out iFirstNumber);
            iSecondNumber = Convert.ToInt32(sSecondNumber);

            iSum = iFirstNumber + iSecondNumber;
            iDifference = iFirstNumber - iSecondNumber;
            iProduct = iFirstNumber * iSecondNumber;
            iQuotient = iFirstNumber / iSecondNumber;
            iRemainder = iFirstNumber % iSecondNumber;
            

            //Display sum
            MessageBox.Show(sFirstNumber + " + " + sSecondNumber + " = " + iSum, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            //Display difference
            MessageBox.Show(sFirstNumber + " - " + sSecondNumber + " = " + iDifference, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            //Display product
            MessageBox.Show(sFirstNumber + " * " + sSecondNumber + " = " + iProduct, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            //Display quotient
            MessageBox.Show(sFirstNumber + " / " + sSecondNumber + " = " + iQuotient, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            //Display remainder
            MessageBox.Show(sFirstNumber + " % " + sSecondNumber + " = " + iRemainder, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);



            //Determine if greater or less than iFirstNumber
            if (iFirstNumber > iSecondNumber)
            {
                MessageBox.Show(sFirstNumber + " is greater than " + iSecondNumber);
            }
            else
            {
                MessageBox.Show(sFirstNumber + " is not greater than " + iSecondNumber);
            }

            if (iFirstNumber < iSecondNumber)
            {
                MessageBox.Show(sFirstNumber + " is less than " + iSecondNumber);
            }
            else
            {
                MessageBox.Show(sFirstNumber + " is not less than " + iSecondNumber);
            }

            if (iFirstNumber == iSecondNumber)
            {
                MessageBox.Show(sFirstNumber + " is equal to " + iSecondNumber);
            }
            else
            {
                MessageBox.Show(sFirstNumber + " is not equal to " + iSecondNumber);
            }

        }
    }
}
