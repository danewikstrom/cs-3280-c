﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace Assignment1Program2
{
    /// <summary>
    /// This is a class that allows the user to enter two numbers and then prints the results of 
    /// addition, subtraction, multiplication, division, and the remainder of the two numbers.  
    /// Then the program prints whether the first number is less than the second number, 
    /// then prints whether the first number is greater than the second number, then prints whether 
    /// the first number equals the second number.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //Create variables
            string sFirstNumber;
            string sSecondNumber;
            int iFirstNumber;
            int iSecondNumber;
            int iSum;
            int iDifference;
            int iProduct;
            int iQuotient;
            int iRemainder;


            //Get the numbers and check if the user input is a valid integer.
            //While the input is not valid, ask user to enter a valid integer
            Console.Write("Enter First Number: ");
            sFirstNumber = Console.ReadLine();
            
            bool bIsValid1 = Int32.TryParse(sFirstNumber, out iFirstNumber);
            while (bIsValid1 == false)
            {
                Console.Write("Please enter a valid integer.");
                sFirstNumber = Console.ReadLine();
                bIsValid1 = Int32.TryParse(sFirstNumber, out iFirstNumber);
            }

            Console.Write("Enter Second Number: ");
            sSecondNumber = Console.ReadLine();

            bool bIsValid2 = Int32.TryParse(sSecondNumber, out iSecondNumber);
            while (bIsValid2 == false)
            {
                Console.Write("Please enter a valid integer.");
                sSecondNumber = Console.ReadLine();
                bIsValid2 = Int32.TryParse(sSecondNumber, out iSecondNumber);
            }
            Console.WriteLine("\n");

            //Convert user input to 32 bit integers 
            Int32.TryParse(sFirstNumber, out iFirstNumber);
            Int32.TryParse(sSecondNumber, out iSecondNumber);

            //assign values to variables
            iSum = iFirstNumber + iSecondNumber;
            iDifference = iFirstNumber - iSecondNumber;
            iProduct = iFirstNumber * iSecondNumber;
            iQuotient = iFirstNumber / iSecondNumber;
            iRemainder = iFirstNumber % iSecondNumber;

            //Display sum
            Console.WriteLine(sFirstNumber + " + " + sSecondNumber + " = " + iSum);
            //Display difference
            Console.WriteLine(sFirstNumber + " - " + sSecondNumber + " = " + iDifference);
            //Display product
            Console.WriteLine(sFirstNumber + " * " + sSecondNumber + " = " + iProduct);
            //Display quotient
            Console.WriteLine(sFirstNumber + " / " + sSecondNumber + " = " + iQuotient);
            //Display remainder
            Console.WriteLine(sFirstNumber + " % " + sSecondNumber + " = " + iRemainder);
            Console.WriteLine("\n");


            //Determine if iSecondNumber is greater than, less than, or equal to iFirstNumber
            if (iFirstNumber > iSecondNumber)
            {
                Console.WriteLine(sFirstNumber + " is greater than " + iSecondNumber);
            }
            else
            {
                Console.WriteLine(sFirstNumber + " is not greater than " + iSecondNumber);
            }

            if (iFirstNumber < iSecondNumber)
            {
                Console.WriteLine(sFirstNumber + " is less than " + iSecondNumber);
            }
            else
            {
                Console.WriteLine(sFirstNumber + " is not less than " + iSecondNumber);
            }

            if (iFirstNumber == iSecondNumber)
            {
                Console.WriteLine(sFirstNumber + " is equal to " + iSecondNumber);
            }
            else
            {
                Console.WriteLine(sFirstNumber + " is not equal to " + iSecondNumber);
            }
            Console.WriteLine("\n");
        }
    }
}