﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1Program1
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Method to display text entered in by user in a message box with yes/no buttons
        /// and will update the label on the form with what button the user entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //This variable is of type "DialogResult".  It tells you which button the user clicked on the message box.
            DialogResult MyResult;

            //This statement does a few things.  First the "Show" method is called to pop up a message box.  Then we pass the "Show"
            //method the text from the text box.  Next we tell it what the caption should be.  Then we tell it what type of buttons
            //we want on the message box.  Then we tell it what kind of icon we want on the message box.  When the user clicks a 
            //button on the message box the result goes into the variable MyResult.
            MyResult = MessageBox.Show ("You typed: " + textBox1.Text, "Display Text", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //We can then print out which button the user clicked.
            label1.Text = "You clicked the " + MyResult.ToString() + " button";
        }

        /// <summary>
        /// Method to display text entered in by user in a message box with OK/cancel buttons
        /// and will update the label on the form with what button the user entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            //This variable is of type "DialogResult".  It tells you which button the user clicked on the message box.
            DialogResult MyResult;

            //This statement does a few things.  First the "Show" method is called to pop up a message box.  Then we pass the "Show"
            //method the text from the text box.  Next we tell it what the caption should be.  Then we tell it what type of buttons
            //we want on the message box.  Then we tell it what kind of icon we want on the message box.  When the user clicks a 
            //button on the message box the result goes into the variable MyResult.
            MyResult = MessageBox.Show("You typed: " + textBox2.Text, "Display Text", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);

            //We can then print out which button the user clicked.
            label2.Text = "You clicked the " + MyResult.ToString() + " button";
        }

        /// <summary>
        /// Method to display text entered in by user in a message box with a OK button
        /// and will update the label on the form to state that the user clicked the OK button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            //This variable is of type "DialogResult".  It tells you which button the user clicked on the message box.
            DialogResult MyResult;

            //This statement does a few things.  First the "Show" method is called to pop up a message box.  Then we pass the "Show"
            //method the text from the text box.  Next we tell it what the caption should be.  Then we tell it what type of buttons
            //we want on the message box.  Then we tell it what kind of icon we want on the message box.  When the user clicks a 
            //button on the message box the result goes into the variable MyResult.
            MyResult = MessageBox.Show("You typed: " + textBox3.Text, "Display Text", MessageBoxButtons.OK, MessageBoxIcon.None);

            //We can then print out which button the user clicked.
            label3.Text = "You clicked the " + MyResult.ToString() + " button";
        }
    }
}
