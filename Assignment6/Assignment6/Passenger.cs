﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
using System.Data.OleDb;
using System.Windows;

namespace Assignment6
{
    /// <summary>
    /// Holds data for a passenger
    /// </summary>
    public class Passenger
    {
        /// <summary>
        /// Passenger objects passenger ID
        /// </summary>
        public string passengerID;
        /// <summary>
        /// Passenger objects passenger first name
        /// </summary>
        public string passengerFirstName;
        /// <summary>
        /// Passenger objects passenger last name
        /// </summary>
        public string passengerLastName;
        /// <summary>
        /// Passenger objects passenger flight ID
        /// </summary>
        public string passengerFlightID;
        /// <summary>
        /// Passenger objects passenger seat
        /// </summary>
        public string passengerSeat;
        /// <summary>
        /// Holds number of returned values 
        /// </summary>
        public int numReturnValuesPassenger;//Number of returned values

        public Passenger()
        {
            passengerID = string.Empty;
            passengerFirstName = string.Empty;
            passengerLastName = string.Empty;
            passengerFlightID = string.Empty;
            passengerSeat = string.Empty;
            numReturnValuesPassenger = 0;
        }

        /// <summary>
        /// Called to load passengers into the choose passenger combo box
        /// </summary>
        /// <returns>List of passenger objects</returns>
        public List<Passenger> getPassengers(Flight aFlight)
        {
            try
            {
                clsDataAccess dbPassengers = new clsDataAccess();
                List<Passenger> passengerList = new List<Passenger>();
                DataSet dsPassenger = new DataSet();//Holds the return value

                string SQLPassengers; //Holds an SQL Statement
                string flightID = aFlight.flightID;
                string flightAircraftType = aFlight.flightAircraftType;

                //Create the SQL statement to extract the passengers
                SQLPassengers = "SELECT PASSENGER.Passenger_ID, First_Name, Last_Name, Seat_Number " +
                                "FROM FLIGHT_PASSENGER_LINK, FLIGHT, PASSENGER " +
                                "WHERE FLIGHT.FLIGHT_ID = FLIGHT_PASSENGER_LINK.FLIGHT_ID AND " +
                                "FLIGHT_PASSENGER_LINK.PASSENGER_ID = PASSENGER.PASSENGER_ID AND " +
                                "Flight.Flight_ID = " + flightID;

                //Extract the passengers and put them into the DataSet
                dsPassenger = dbPassengers.ExecuteSQLStatement(SQLPassengers, ref numReturnValuesPassenger);

                //Loop through the data and create passenger classes
                for (int i = 0; i < numReturnValuesPassenger; i++)
                {
                    Passenger aPassenger = new Passenger();

                    aPassenger.passengerID = dsPassenger.Tables[0].Rows[i][0].ToString();
                    aPassenger.passengerFirstName = dsPassenger.Tables[0].Rows[i]["First_Name"].ToString();
                    aPassenger.passengerLastName = dsPassenger.Tables[0].Rows[i]["Last_Name"].ToString();
                    aPassenger.passengerSeat = dsPassenger.Tables[0].Rows[i][3].ToString();
                    passengerList.Add(aPassenger);
                }
                return passengerList;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
            
        }

        /// <summary>
        /// Override the toString method so that this string is displayed in the combo box.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            try
            {
                return passengerFirstName + " " + passengerLastName;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}

