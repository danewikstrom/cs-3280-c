﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
using System.Windows;

namespace Assignment6
{
    /// <summary>
    /// Holds data for a flight
    /// </summary>
    public class Flight
    {
        /// <summary>
        /// Flight objects flight ID
        /// </summary>
        public string flightID;
        /// <summary>
        /// Flight objects flight number
        /// </summary>
        public string flightNumber;
        /// <summary>
        /// Flight objects aircraft type
        /// </summary>
        public string flightAircraftType;
        /// <summary>
        /// Flight objects passenger ID
        /// </summary>
        public string flightPassengerID;
        /// <summary>
        /// Flight objects passenger seat number
        /// </summary>
        public string flightSeatNumber;
        /// <summary>
        /// Holds number of returned values 
        /// </summary>
        public int numReturnValuesFlight;
        public Flight()
        {
            flightID = string.Empty;
            flightNumber = string.Empty;
            flightAircraftType = string.Empty;
            flightPassengerID = string.Empty;
            flightSeatNumber = string.Empty;
            numReturnValuesFlight = 0;
        }
        /// <summary>
        /// Called to load flights into the choose flight combo box
        /// </summary>
        /// <returns>List of flight objects</returns>
        public List<Flight> getFlights()
        {
            try
            {
                clsDataAccess dbFlight = new clsDataAccess(); ;
                List<Flight> flightList = new List<Flight>();
                DataSet dsFlight = new DataSet();//Holds the return value

                string SQLFlights;//Holds and SQL statement

                //Create the SQL statement to extract the flights
                SQLFlights = "SELECT Flight_ID, Flight_Number, Aircraft_Type " +
                             "FROM Flight";

                //Extract the flights and put them into the DataSet
                dsFlight = dbFlight.ExecuteSQLStatement(SQLFlights, ref numReturnValuesFlight);

                //Loop through the data and create flight classes
                for (int i = 0; i < numReturnValuesFlight; i++)
                {
                    Flight aFlight = new Flight(); ;

                    aFlight.flightID = dsFlight.Tables[0].Rows[i][0].ToString();
                    aFlight.flightNumber = dsFlight.Tables[0].Rows[i][1].ToString();
                    aFlight.flightAircraftType = dsFlight.Tables[0].Rows[i][2].ToString();

                    flightList.Add(aFlight);
                }
                return flightList;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        public override string ToString()
        {
            try
            {
                return flightNumber + "-" + flightAircraftType;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}

