﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Assignment6
{
    /// <summary>
    /// Interaction logic for wndAddPassenger.xaml
    /// </summary>
    public partial class wndAddPassenger : Window
    {
        /// <summary>
        /// Holds value if user saved and submitted a new passenger
        /// </summary>
        public bool didSumbit;
        /// <summary>
        /// Holds new passengers first name
        /// </summary>
        public string firstName;
        /// <summary>
        /// Holds new passengers last name
        /// </summary>
        public string lastName;

        public wndAddPassenger()
        {
            InitializeComponent();

            didSumbit = false;
        }
        /// <summary>
        /// Executes when user clicks the save button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                saveName();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when the user clicks the cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when each key is pressed when user is entering a first name into the text box.
        /// Calls nameValidation method to validate input.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void firstNameTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                nameValidation(sender, e);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when each key is pressed when user is entering a first name into the text box.
        /// Calls nameValidation method to validate input.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lastNameTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                nameValidation(sender, e);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Validates users input. Only accepts letters, backspaces, spaces, and tabs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nameValidation(object sender, KeyEventArgs e)
        {
            try
            {
                if (!(e.Key >= Key.A && e.Key <= Key.Z))
                {
                    //Allow the user to use the backspace, delete, enter, space, and tab
                    if (!(e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.Tab))
                    {
                        //No other keys allowed besides numbers, backspace, enter/return, and delete
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that occur when the user clicks the save button
        /// </summary>
        private void saveName()
        {
            try
            {
                firstName = firstNameTextBox.Text;
                lastName = lastNameTextBox.Text;
                didSumbit = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
