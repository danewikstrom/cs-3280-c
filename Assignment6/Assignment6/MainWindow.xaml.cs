﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.OleDb;
using System.Reflection;

namespace Assignment6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Flight object of the selected flight
        /// </summary>
        public Flight selectedFlight;
        /// <summary>
        /// Flight object of the selected passenger
        /// </summary>
        public Passenger selectedPassenger;
        /// <summary>
        /// list of the current flight objects
        /// </summary>
        public List<Flight> flightList;
        /// <summary>
        /// list of the current passenger objects
        /// </summary>
        public List<Passenger> passengerList;
        /// <summary>
        /// Holds value is a flight has been selected
        /// </summary>
        bool flightIsSelected;
        
        public MainWindow()
        {
            InitializeComponent();

            selectedFlight = new Flight();
            selectedPassenger = new Passenger();
            flightIsSelected = false;
            flightList = new List<Flight>();
            passengerList = new List<Passenger>();
            loadFlights();
        }
        /// <summary>
        /// Executes when user selects a flight from the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)//flight
        {
            try
            {
                chooseFlight();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when user selects a passenger from the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)//passenger
        {
            try
            {
                choosePassenger();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when the user selects the add passenger button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addPassengerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                addPassenger();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the add passenger button is clicked
        /// </summary>
        private void addPassenger()
        {
            try
            {
                wndAddPassenger addPassengerWindow = new wndAddPassenger();
                addPassengerWindow.ShowDialog();

                if (addPassengerWindow.didSumbit)
                {

                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen to load flights into the combo box
        /// </summary>
        private void loadFlights()
        {
            try
            {
                flightList = selectedFlight.getFlights();

                for (int i = 0; i < selectedFlight.numReturnValuesFlight; i++)
                {
                    chooseFlightComboBox.Items.Add(flightList[i]);
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen to load passengers into the combo box
        /// </summary>
        private void loadPassengers()
        {
            try
            {
                passengerList = selectedPassenger.getPassengers(selectedFlight);

                for (int i = 0; i < selectedPassenger.numReturnValuesPassenger; i++)
                {
                    choosePassengerComboBox.Items.Add(passengerList[i]);
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user selects a flight
        /// </summary>
        private void chooseFlight()
        {
            try
            {
                selectedFlight = (Flight)chooseFlightComboBox.SelectedItem;
                loadPassengers();

                if (selectedFlight.flightAircraftType == "Boeing 787")
                {
                    _737Label.Visibility = Visibility.Collapsed;
                    _737WrapPanel.Visibility = Visibility.Collapsed;
                    _787Label.Visibility = Visibility.Visible;
                    _787WrapPanel.Visibility = Visibility.Visible;
                }
                else if (selectedFlight.flightAircraftType == "Boeing 737")
                {
                    _787Label.Visibility = Visibility.Collapsed;
                    _787WrapPanel.Visibility = Visibility.Collapsed;
                    _737Label.Visibility = Visibility.Visible;
                    _737WrapPanel.Visibility = Visibility.Visible;
                }
                flightIsSelected = true;
                choosePassengerComboBox.IsEnabled = true;
                addPassengerButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user selects a passenger
        /// </summary>
        private void choosePassenger()
        {
            try
            {
                //Extract the selected Passenger object from the combo box
                selectedPassenger = (Passenger)choosePassengerComboBox.SelectedItem;

                //Set the seat label
                passengersSeatNumberLabel.Content = selectedPassenger.passengerSeat;

                if (selectedFlight.flightAircraftType == "Boeing 737")
                {
                    //Need to find the selected seat in the panel.  Loop through each label in the panel.
                    foreach (Button MySeatButton in _737WrapPanel.Children)
                    {
                        MySeatButton.Background = Brushes.Blue;

                        //Check to see if the passenger's seat matches the label
                        if (MySeatButton.Content.ToString() == selectedPassenger.passengerSeat)
                            MySeatButton.Background = Brushes.Green;
                    }
                }
                if (selectedFlight.flightAircraftType == "Boeing 787")
                {
                    //Need to find the selected seat in the panel.  Loop through each label in the panel.
                    foreach (Button MySeatButton in _787WrapPanel.Children)
                    {
                        MySeatButton.Background = Brushes.Blue;

                        //Check to see if the passenger's seat matches the label
                        if (MySeatButton.Content.ToString() == selectedPassenger.passengerSeat)
                            MySeatButton.Background = Brushes.Green;
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user selects a new flight
        /// </summary>
        private void changeFlight()
        {
            try
            {
                passengersSeatNumberLabel.Content = string.Empty;
                choosePassengerComboBox.Items.Clear();
                choosePassengerComboBox.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
