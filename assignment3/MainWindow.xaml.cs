﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace assignment3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// Sets cursor to NumOfStudentsTextBox
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            NumOfStudentsTextBox.Focus();
            NumOfStudentsTextBox.Select(0, 0);//Puts cursor in NumOfStudentsTextBox
        }

#region Member Variables
        /// <summary>
        /// Holds the number of students the user entered
        /// </summary>
        int numOfStudents = 0;
        /// <summary>
        /// Holds the number of assignments the user entered
        /// </summary>
        int numOfAssignments = 0;
        /// <summary>
        /// Keeps track of the current position in the students array
        /// </summary>
        int arrayPosition = 1;
        /// <summary>
        /// Holds the number of the assignment number the user entered
        /// </summary>
        int assignmentNumber = 0;
        /// <summary>
        /// Holds the value of the assignment score the user entered
        /// </summary>
        double assignmentScore = 0;
        /// <summary>
        /// Used to display the student's score information 
        /// </summary>
        string display = "";
        /// <summary>
        /// Holds the values of all student's scores
        /// </summary>
        double[,] scores = new double[10, 99];
        /// <summary>
        /// Holds the values of all student's average scores
        /// </summary>
        double[] average = new double[10];
        /// <summary>
        /// Holds the assigned names of all students
        /// </summary>
        string[] students = new string[10];
        /// <summary>
        /// Holds the letter grades of all students
        /// </summary>
        string[] letterGrades = new string[10];
        #endregion
#region SubmitCountButton_Click Function
        /// <summary>
        /// Validates values of number of students and assignments entered by the user.
        /// Validates that the number of students and assignment text boxes were filled with a value.
        /// Defaults all student names to "Student #1", "Student #2", etc. and updates student name label.
        /// Defaults all student scores to 0.
        /// Updates the number of assignments label to reflect the number of assignments the user entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitCountsButton_Click(object sender, RoutedEventArgs e)
        {
            Int32.TryParse(NumOfStudentsTextBox.Text, out numOfStudents);//convert number of students to an integer
            Int32.TryParse(NumOfAssignmentsTextBox.Text, out numOfAssignments);//convert number of assignments to an integer

            //check if values were entered in the number of students and assignments text boxes
            if (String.IsNullOrEmpty(NumOfStudentsTextBox.Text) || String.IsNullOrEmpty(NumOfAssignmentsTextBox.Text))
                DisplayIncompleteMessageBox();
            //check if value entered for number of students is between 1-10
            else if (numOfStudents > 10 || numOfStudents < 1)
            {
                MessageBoxResult result = MessageBox.Show("Number of students must be between 1 - 10.", "Invalid");
            }
            //check if value entered for number of assignments is between 1-99
            else if(numOfAssignments > 99 || numOfAssignments < 1)
            {
                MessageBoxResult result = MessageBox.Show("Number of assignment must be between 1 - 99.", "Invalid");
            }
            else
            {
                //update assignment number label
                AssignmentNumberLabel.Content = ("Enter Assignment Number (1-" + numOfAssignments.ToString() + "):");

                for (int i = 0; i < students.Length; i++)//set student names
                    students[i] = "Student #" + (i + 1);

                NumOfStudentsTextBox.Clear();
                NumOfAssignmentsTextBox.Clear();
                StudentNameOrNumLabel.Content = students[0];//default to student #1 on student name label

                for (int row = 0; row < numOfStudents; row++)//initialize needed score's indexes to 0
                {
                    for (int column = 0; column < numOfAssignments; column++)
                    {
                        scores[row, column] = 0;
                    }
                }
            }
        }
        #endregion
#region FirstStudentButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Changes student selection to the first student in students array.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FirstStudentButton_Click(object sender, RoutedEventArgs e)
        {
            if(numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            else
            {
                StudentNameOrNumLabel.Content = students[0];
                arrayPosition = 1;
            }
        }
        #endregion
#region PrevStudentButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Changes student selection to the previous student in the students array.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrevStudentButton_Click(object sender, RoutedEventArgs e)
        {
            if (numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            else if (arrayPosition == 1)//if current student is the first student, stay there
                StudentNameOrNumLabel.Content = students[0];
            else
            {
                arrayPosition = (arrayPosition - 1);
                StudentNameOrNumLabel.Content = students[arrayPosition - 1];
            }  
        }
        #endregion
#region NextStudentButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Changes student selection to the next student in the students array.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextStudentButton_Click(object sender, RoutedEventArgs e)
        {
            if (numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            else if (arrayPosition == numOfStudents)//if current student is the last student, stay there
                StudentNameOrNumLabel.Content = students[numOfStudents - 1];
            else
            {
                arrayPosition = (arrayPosition + 1);
                StudentNameOrNumLabel.Content = students[arrayPosition - 1];
            }
        }
        #endregion
#region LastStudentButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Changes student selection to the last student in the students array.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastStudentButton_Click(object sender, RoutedEventArgs e)
        {
            if (numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            else
            {
                StudentNameOrNumLabel.Content = students[numOfStudents - 1];
                arrayPosition = numOfStudents;
            }
        }
        #endregion
#region SaveNameButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Saves the name the user entered to the selected student.
        /// Updates student name label and student name in students array.
        /// Clears text box for new entry.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveNameButton_Click(object sender, RoutedEventArgs e)
        {
            if (numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            else
                students[arrayPosition - 1] = StudentNameTextBox.Text;

            StudentNameOrNumLabel.Content = StudentNameTextBox.Text;
            StudentNameTextBox.Clear();
        }
        #endregion
#region SaveScoreButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Validates that the assignment number and score text boxes were filled with a value.
        /// Updates scores array with a students score for a particular assignment.
        /// Clears assignment number text box for a new value to be entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveScoreButton_Click(object sender, RoutedEventArgs e)
        {
            if (numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            //check if values were entered in the number of students and assignments text boxes
            else if (String.IsNullOrEmpty(AssignmentNumberTextBox.Text) || String.IsNullOrEmpty(AssignmentScoreTextBox.Text))
            {
                MessageBoxResult result = MessageBox.Show("Enter assignment score and number then click 'Save Score'.", "Incomplete");
            }
            else
            {
                Double.TryParse(AssignmentScoreTextBox.Text, out assignmentScore);//convert assignment score to a double
                Int32.TryParse(AssignmentNumberTextBox.Text, out assignmentNumber);//convert assignment number to an integer
                //check if assignment number is within specified range
                if (assignmentNumber > numOfAssignments || assignmentNumber < 1)
                {
                    MessageBoxResult result = MessageBox.Show("Assignment number does not exist.", "Invalid");
                    assignmentScore = 0;
                    assignmentNumber = 0;
                }
                else
                    scores[(arrayPosition - 1), (assignmentNumber - 1)] = assignmentScore;//save score in scores array
            }
            AssignmentNumberTextBox.Clear();//clear assignment number text box
            AssignmentNumberTextBox.Focus();
            AssignmentNumberTextBox.Select(0, 0);//set cursor to assignment number text box
        }
        #endregion
#region ResetScoresButton_Click Function
        /// <summary>
        /// Resets all member variables, labels and textBoxes to initial states
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetScoresButton_Click(object sender, RoutedEventArgs e)
        {
            numOfStudents = 0;
            numOfAssignments = 0;
            assignmentNumber = 0;
            assignmentScore = 0;
            display = "";
            arrayPosition = 1;

            for (int i = 0; i < students.Length; i++)
            {
                students[i] = "";
                average[i] = 0.0;
                letterGrades[i] = "";
            }
                
            for (int row = 0; row < 10; row++)
            {
                for (int column = 0; column < 99; column++)
                    scores[row, column] = 0;
            }

            NumOfStudentsTextBox.Text = "";
            NumOfAssignmentsTextBox.Text = "";
            StudentNameTextBox.Text = "";
            AssignmentNumberTextBox.Text = "";
            AssignmentScoreTextBox.Text = "";
            StudentNameOrNumLabel.Content = "-";
            AssignmentNumberLabel.Content = "-";
            ScoresTextBox.Text = "Scores";
        }
        #endregion
#region DisplayScoreButton_Click Function
        /// <summary>
        /// Validates that the number of students and assignments has been entered.
        /// Writes students, assignments, average grades, and letter grades to display string
        /// and displays the string in the ScoresTextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayScoreButton_Click(object sender, RoutedEventArgs e)
        {
            if (numOfAssignments == 0 || numOfStudents == 0)
                DisplayIncompleteMessageBox();
            else
            {
                Average(scores);//call Average function to set all averages for each student
                Grade(scores);//call Grade function to set letter grades for each student
                
                //write top row to string
                display = "STUDENT\t";
                for (int i = 1; i <= numOfAssignments; i++)
                    display += "#" + i + "    ";
                display += "AVG   GRADE\n\n";

                for (int row = 0; row < numOfStudents; row++)
                {
                    display += students[row] + "\t";//write student to string

                    for (int column = 0; column < (numOfAssignments); column++)//write scores to string
                        display += scores[row, column] + "    ";
                    //write average grade and letter grade to string
                    display += "  " + (average[row]).ToString("0.##") + "      " + (letterGrades[row]).ToString() + "\n";
                }
                ScoresTextBox.Text = display;//display to scores text box
            }
        }
        #endregion
#region Average Function
        /// <summary>
        /// Calculates the average score for each student and stores the values in the average array
        /// </summary>
        /// <param name="scores"></param>
        private void Average(double[,] scores)
        {
            double sum = 0;

            for(int row = 0; row < numOfStudents; row++)
            {
                for(int column = 0; column < numOfAssignments; column++)
                    sum += scores[row, column];

                average[row] = sum / numOfAssignments;
                sum = 0;
            }
        }
        #endregion
#region Grade Function
        /// <summary>
        /// Calculates the number grade for each student, converts value to a letter grade
        /// and stores the letter grade in the letterGrades array. 
        /// </summary>
        /// <param name="scores"></param>
        private void Grade(double[,] scores)
        {
            int totalPointsPossible = 0;//stores the total points possible
            double pointsEarned = 0;//stores the total points the student earned
            double percentageGrade = 0.0;//stores the numeric grade that can be interpreted as a percentage grade

            totalPointsPossible = numOfAssignments * 100;//calculate and assign total point possible

            for (int row = 0; row < numOfStudents; row++)
            {
                for (int column = 0; column < numOfAssignments; column++)
                    pointsEarned += scores[row, column];//calculate and assign points earned for each student

                percentageGrade = (pointsEarned / totalPointsPossible) * 100;//calculate and assign numeric grade
                letterGrades[row] = DetermineLetterGrade(percentageGrade);//call DetermineLetterGrade() and set values letterGrade array
                pointsEarned = 0;
            }
        }
        #endregion
#region DeterminLetterGrade Function
        /// <summary>
        /// Called by the Grade function to determine the letter grade based on a calculated number grade
        /// </summary>
        /// <param name="percentageGrade"></param>
        /// <returns></returns>
        private string DetermineLetterGrade(double percentageGrade)
        {
            if (percentageGrade <= 100 && percentageGrade >= 93)
                return "A";
            else if (percentageGrade <= 92.9 && percentageGrade >= 90)
                return "A-";
            else if (percentageGrade <= 89.9 && percentageGrade >= 87)
                return "B+";
            else if (percentageGrade <= 86.9 && percentageGrade >= 83)
                return "B";
            else if (percentageGrade <= 82.9 && percentageGrade >= 80)
                return "B-";
            else if (percentageGrade <= 79.9 && percentageGrade >= 77)
                return "C+";
            else if (percentageGrade <= 76.9 && percentageGrade >= 73)
                return "C";
            else if (percentageGrade <= 72.9 && percentageGrade >= 70)
                return "C-";
            else if (percentageGrade <= 69.9 && percentageGrade >= 67)
                return "D+";
            else if (percentageGrade <= 66.9 && percentageGrade >= 63)
                return "D";
            else if (percentageGrade <= 62.9 && percentageGrade >= 60)
                return "D-";
            else
                return "E";
        }
        #endregion
#region DisplayIncompleteMessageBox
        /// <summary>
        /// Called when user has not entered valid data for the number of students and assignments.
        /// Displays a message box informing the user to enter the number of students and assignments.
        /// </summary>
        private void DisplayIncompleteMessageBox()
        {
            MessageBoxResult result = MessageBox.Show("Enter the number of students and assignments then click 'Submit Counts'.", "Incomplete");
        }
#endregion
    }
}
