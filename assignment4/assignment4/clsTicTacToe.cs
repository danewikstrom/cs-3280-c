﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4
{
    /// <summary>
    /// Class that handles all logic associated with the tic-tac-toe game
    /// </summary>
    class clsTicTacToe
    {
#region Class Variables
        /// <summary>
        /// 2d string array that represents the tic-tac-toe board
        /// </summary>
        private string[,] board;
        /// <summary>
        /// Holds the number of wins player 1 has
        /// </summary>
        private int player1NumWins;
        /// <summary>
        /// Holds the number of wins player 2 or the computer has depending on game type
        /// </summary>
        private int player2OrComputerNumWins;
        /// <summary>
        /// Holds the number of ties
        /// </summary>
        private int ties;
        /// <summary>
        /// Represents if player 1 won the round
        /// </summary>
        private bool player1Won;
        /// <summary>
        /// Represents if player 2 or the computer won the round depending on game type
        /// </summary>
        private bool player2OrComputerWon;
        /// <summary>
        /// Represents the possible willing moves
        /// </summary>
        public WinningMove winningMove;
        /// <summary>
        /// Default Constructor
        /// </summary>
#endregion
#region Default Constructor. Variable initialization
        ///Default Constructor. Variable initialization.
        public clsTicTacToe()//constructor
        {
            board = new string[3, 3] {{"", "", ""},
                                      {"", "", ""},
                                      {"", "", ""}};
            player1Won = false;
            player2OrComputerWon = false;
            player1NumWins = 0;
            player2OrComputerNumWins = 0;
            ties = 0;
        }
#endregion
#region enumeration WinningMove
        /// <summary>
        /// Used to determine a winning move
        /// </summary>
        public enum WinningMove 
        {
            row1,
            row2,
            row3,
            col1,
            col2,
            col3,
            diag1,//top left to right bottom
            diag2//top right to left bottom
        }
#endregion
#region IsWinningMove Method
        /// <summary>
        /// Determines if the winning move was a diagonal, vertical, horizontal win or not a win
        /// </summary>
        /// <returns></returns>
        public bool IsWinningMove()
        {
            if (IsDiagonalWin())
                return true;
            if (IsVerticalWin())
                return true;
            if (IsHorizontalWin())
                return true;
            else return false;
        }
#endregion
#region IsTie Method
        /// <summary>
        /// Determines if game was a tie
        /// </summary>
        /// <returns></returns>
        public bool IsTie()
        {
            for(int row = 0; row < 3; row++)//make sure that all spaces have been filled before determining a tie
            {
                for(int column = 0; column < 3; column++)
                {
                    if (board[row, column] == "")
                        return false;
                }
            }
           SetNumberOfTies();
           return true;
        }
#endregion
#region IsDiagonalWin Method
        /// <summary>
        /// Determines if the win is a diagonal win, what player made the win, and what diagonal win was made.
        /// Updates a particular player's number of wins.
        /// </summary>
        /// <returns></returns>
        private bool IsDiagonalWin()
        {
            if (board[0, 0] == "X" && board[1, 1] == "X" && board[2, 2] == "X")//diag1
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.diag1;
                return true;
            }
            if (board[0, 0] == "O" && board[1, 1] == "O" && board[2, 2] == "O")//diag1
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.diag1;
                return true;
            }
            if (board[0, 2] == "X" && board[1, 1] == "X" && board[2, 0] == "X")//diag2
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.diag2;
                return true;
            }
            if (board[0, 2] == "O" && board[1, 1] == "O" && board[2, 0] == "O")//diag2
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.diag2;
                return true;
            }
            else return false; 
        }
#endregion
#region IsVerticalWin Method
        /// <summary>
        /// Determines if the win is a vertical win, what player made the win, and what column win was made.
        /// Updates a particular player's number of wins.
        /// </summary>
        /// <returns></returns>
        private bool IsVerticalWin()
        {
            if (board[0, 0] == "X" && board[1, 0] == "X" && board[2, 0] == "X")//col1
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.col1;
                return true;
            }
            if (board[0, 0] == "O" && board[1, 0] == "O" && board[2, 0] == "O")//col1
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.col1;
                return true;
            }
            if (board[0, 1] == "X" && board[1, 1] == "X" && board[2, 1] == "X")//col2
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.col2;
                return true;
            }
            if (board[0, 1] == "O" && board[1, 1] == "O" && board[2, 1] == "O")//col2
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.col2;
                return true;
            }
            if (board[0, 2] == "X" && board[1, 2] == "X" && board[2, 2] == "X")//col3
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.col3;
                return true;
            }
            if (board[0, 2] == "O" && board[1, 2] == "O" && board[2, 2] == "O")//col3
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.col3;
                return true;
            }
            else return false;
        }
#endregion
#region IsHorizontalWin Method
        /// <summary>
        /// Determines if the win is a horizontal win, what player made the win, and what row win was made.
        /// Updates a particular player's number of wins.
        /// </summary>
        /// <returns></returns>
        private bool IsHorizontalWin()
        {
            if (board[0, 0] == "X" && board[0, 1] == "X" && board[0, 2] == "X")//row1
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.row1;
                return true;
            }
            if (board[0, 0] == "O" && board[0, 1] == "O" && board[0, 2] == "O")//row1
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.row1;
                return true;
            }
            if (board[1, 0] == "X" && board[1, 1] == "X" && board[1, 2] == "X")//row2
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.row2;
                return true;
            }
            if (board[1, 0] == "O" && board[1, 1] == "O" && board[1, 2] == "O")//row2
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.row2;
                return true;
            }
            if (board[2, 0] == "X" && board[2, 1] == "X" && board[2, 2] == "X")//row3
            {
                SetNumberOfWinsPlayer1();
                SetPlayer1AsWinner();
                winningMove = WinningMove.row3;
                return true;
            }
            if (board[2, 0] == "O" && board[2, 1] == "O" && board[2, 2] == "O")//row3
            {
                SetNumberOfWinsPlayer2();
                SetPlayer2OrComputeAsWinner();
                winningMove = WinningMove.row3;
                return true;
            }
            else return false;
        }
#endregion
#region SetSpaceToX Method
        /// <summary>
        /// Sets the board used for logic to what the player 1 has chosen
        /// </summary>
        /// <param name="buttonName"></param>
        public void SetSpaceToX(string buttonName)
        {
            for (int row = 0; row < 3; row++)//set board used for logic the same as what is on UI
            {
                for (int column = 0; column < 3; column++)
                {
                    if (buttonName == "r" + row + "c" + column)
                        board[row, column] = "X";
                }
            }
        }
#endregion
#region SetSpaceToO Method
        /// <summary>
        /// Sets the board used for logic to what the player 2 or the computer has chosen
        /// </summary>
        /// <param name="buttonName"></param>
        public void SetSpaceToO(string buttonName)
        {
            for (int row = 0; row < 3; row++)//set board used for logic the same as what is on UI
            {
                for (int column = 0; column < 3; column++)
                {
                    if (buttonName == "r" + row + "c" + column)
                        board[row, column] = "O";
                }
            }
        }
#endregion
#region ComputerChoice Method
        /// <summary>
        /// AI used to determine if the computer needs to block a potential win or choose any available space
        /// </summary>
        /// <returns></returns>
        public string ComputerChoice()
        {
            string [,] blockTestBoard = new string[3, 3];//test board to determine if a block is needed
            string buttonToBeSetToComputerChoice;

            for (int row = 0; row < 3; row++)//set blockTestBoard = to board
            {
                for(int column = 0; column < 3; column++)
                {
                    blockTestBoard[row, column] = board[row, column];
                }
            }

            for (int row = 0; row < 3; row++)//test if computer needs to block a potential win
            {
                for (int column = 0; column < 3; column++)
                {
                    if (board[row, column] == "")//if empty see if it needs to be blocked
                    {
                        if (CheckIfComputerNeedsToBlock(blockTestBoard, row, column))//if it results in a possible win
                            return buttonToBeSetToComputerChoice = "r" + row.ToString() + "c" + column.ToString();
                    }
                }
            }

            for (int row = 0; row < 3; row++)//no block so set to next available empty space
            {
                for (int column = 0; column < 3; column++)
                {       
                    //if nothing needs to be blocked and space is empty then choose that space
                    if (board[row, column] != "X" && board[row, column] != "O")
                        return buttonToBeSetToComputerChoice = "r" + row.ToString() + "c" + column.ToString();
                }
            }
            return "Error";//if this happens then there is an error
        }
#endregion
#region CheckIfComputerNeedsToBlock Method
        /// <summary>
        /// Used in conjunction with ComputerChoice() to determine if the computer need to block a potential win.
        /// Evaluates if player 1 made a particular move if it would be a win.
        /// </summary>
        /// <param name="blockTestBoard"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private bool CheckIfComputerNeedsToBlock(string[,] blockTestBoard, int row, int column)
        {
            blockTestBoard[row, column] = "X";//temporarily set this spot to X to test if a block is needed

            //diagonal
            if (blockTestBoard[0, 0] == "X" && blockTestBoard[1, 1] == "X" && blockTestBoard[2, 2] == "X")//diag1
            {
                blockTestBoard[row, column] = "";
                return true;
            }
            if (blockTestBoard[0, 2] == "X" && blockTestBoard[1, 1] == "X" && blockTestBoard[2, 0] == "X")//diag2
            {
                blockTestBoard[row, column] = "";
                return true;
            }

            //vertical
            if (blockTestBoard[0, 0] == "X" && blockTestBoard[1, 0] == "X" && blockTestBoard[2, 0] == "X")//col1
            {
                blockTestBoard[row, column] = "";
                return true;
            }
            if (blockTestBoard[0, 1] == "X" && blockTestBoard[1, 1] == "X" && blockTestBoard[2, 1] == "X")//col2
            {
                blockTestBoard[row, column] = "";
                return true;
            }
            if (blockTestBoard[0, 2] == "X" && blockTestBoard[1, 2] == "X" && blockTestBoard[2, 2] == "X")//col3
            {
                blockTestBoard[row, column] = "";
                return true;
            }

            //horizontal
            if (blockTestBoard[0, 0] == "X" && blockTestBoard[0, 1] == "X" && blockTestBoard[0, 2] == "X")//row1
            {
                blockTestBoard[row, column] = "";
                return true;
            }
            if (blockTestBoard[1, 0] == "X" && blockTestBoard[1, 1] == "X" && blockTestBoard[1, 2] == "X")//row2
            {
                blockTestBoard[row, column] = "";
                return true;
            }
            if (blockTestBoard[2, 0] == "X" && blockTestBoard[2, 1] == "X" && blockTestBoard[2, 2] == "X")//row3
            {
                blockTestBoard[row, column] = "";
                return true;
            }

            blockTestBoard[row, column] = "";
            return false; 
    }
#endregion
#region ResetSpaces Method
        public void ResetSpaces()//resets board used to logic once a new game starts
        {
            for (int row = 0; row < 3; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    board[row, column] = "";
                }
            }
        }
#endregion
# region ResetStats Method
        /// <summary>
        /// Resets variables used for logic to their initial state
        /// </summary>

        public void ResetStats()
        {
            player1NumWins = 0;
            player2OrComputerNumWins = 0;
            ties = 0;
        }
        #endregion
#region ResetWinner method
        /// <summary>
        /// Resets who won
        /// </summary>
        public void ResetWinner()
        {
            player1Won = false;
            player2OrComputerWon = false;
        }
#endregion
        #region Setter Methods
        /// <summary>
        /// Setter method to update player1NumWins variable
        /// </summary>
        private void SetNumberOfWinsPlayer1()
        {
            player1NumWins++;
        }
        /// <summary>
        /// Setter method to update player2OrComputerNumWins variable
        /// </summary>
        private void SetNumberOfWinsPlayer2()
        {
            player2OrComputerNumWins++;
        }
        /// <summary>
        /// Setter method to update ties variable
        /// </summary>
        private void SetNumberOfTies()
        {
            ties++;
        }
        /// <summary>
        /// Setter method to update player 1 as the winner for the round
        /// </summary>
        private void SetPlayer1AsWinner()
        {
            player1Won = true;
        }
        /// <summary>
        /// Setter method to update player 2 or the computer as the winner for the round
        /// </summary>
        private void SetPlayer2OrComputeAsWinner()
        {
            player2OrComputerWon = true;
        }
#endregion
#region Getter Methods
        /// <summary>
        /// Getter method to return player 1's number of wins
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfWinsPlayer1()
        {
            return player1NumWins;
        }
        /// <summary>
        /// Getter method to return player 2's  or the computers number of wins
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfWinsPlayer2OrComputer()
        {
            return player2OrComputerNumWins;
        }
        /// <summary>
        /// Getter method to return the number of ties
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfTies()
        {
            return ties;
        }
        /// <summary>
        /// Getter method to return if player 1 won 
        /// </summary>
        /// <returns></returns>
        public bool GetIfPlayer1Won()
        {
            return player1Won;
        }
        /// <summary>
        /// Getter method to return if player 2 or computer won 
        /// </summary>
        /// <returns></returns>
        public bool GetIfPlayer2OrComputerWon()
        {
            return player2OrComputerWon;
        }
        /// <summary>
        /// Resets the board used for logic to its initial empty state
        /// </summary>
#endregion
    }
}
