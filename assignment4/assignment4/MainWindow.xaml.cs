﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assignment4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
#region Variables
        /// <summary>
        /// instance of clsTicTacToe
        /// </summary>
        clsTicTacToe clsTicTac;
        /// <summary>
        /// represents if the 'Start A New Game' button has been clicked
        /// </summary>
        bool isGameStarted;
        /// <summary>
        /// represents if the game type is in two player mode
        /// </summary>
        bool twoPlayer;
        /// <summary>
        /// represents if the game type is in computer mode
        /// </summary>
        bool computer;
        /// <summary>
        /// represents if player 1 is the winner of the game
        /// </summary>
        bool player1IsTheWinner;
        /// <summary>
        /// represents if player 2 or the computer (depending on game type) is the winner of the game
        /// </summary>
        bool player2OrComputerIsTheWinner;
        /// <summary>
        /// represents if the game resulted in a tie
        /// </summary>
        bool tieGame;
        /// <summary>
        /// represents if the computer won the previous round and will make the first move on next round
        /// </summary>
        bool computerMakesFirstMove;
        /// <summary>
        /// represents whose turn it is
        /// </summary>
        WhoseTurn whoseTurn;
#endregion
#region MainWindow variable initialization
        public MainWindow()
        {
            InitializeComponent();
            
            clsTicTac = new clsTicTacToe();
            isGameStarted = false;
            twoPlayer = false;
            computer = false;
            player1IsTheWinner = false;
            player2OrComputerIsTheWinner = false;
            tieGame = false;
            computerMakesFirstMove = false;
            whoseTurn = WhoseTurn.player1_X;//player 1 always starts first on first round
        }
#endregion
#region enumeration WhoseTurn
        /// <summary>
        /// Used in determining whose turn it is. 
        /// </summary>
        private enum WhoseTurn
        {
            player1_X,//player 1 is always X
            player2_O,//player 2 is always O
            computer_O//computer is always O
        }
#endregion
#region twoPlayerButton_Click Method
        /// <summary>
        /// Method that executes when the 'Two Player' button is clicked.
        /// Checks if 'Computer' button is selected. Displays message if needed.
        /// Updates button and labels associated with button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void twoPlayerButton_Click(object sender, RoutedEventArgs e)
        {
            if(computer == true)//check if computer is already selected
                GamePlayerTypeAlreadySelected();
            else
            {
                twoPlayer = true;
                twoPlayerButton.Background = Brushes.Pink;
                player2OrComputerWinsLabel.Content = "O - Player 2 Wins:";
                gameStatusLabel.Content = "Press 'Start A New Game' Button";
            }
        }
#endregion
#region computerButton_Click Method
        /// <summary>
        /// Method that executes when the 'Computer' button is clicked.
        /// Checks if 'Two Player' button is selected. Displays message if needed.
        /// Updates button, variable, and labels associated with button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void computerButton_Click(object sender, RoutedEventArgs e)
        {
            if(twoPlayer == true)//check if two player is already selected
                GamePlayerTypeAlreadySelected();
            else
            {
                computer = true;
                computerButton.Background = Brushes.Pink;
                player2OrComputerWinsLabel.Content = "O - Computer Wins:";
                gameStatusLabel.Content = "Press 'Start A New Game' Button";
            }
        }
#endregion
#region startButton_Click Method
        /// <summary>
        /// Method that executes when the 'Start A New Game' button is clicked.
        /// Checks if 'Computer' or 'Two Player' button is selected. Displays message if needed.
        /// Updates buttons, variables, and labels associated with button.
        /// Checks if computer is to make the first move. Calls ComputerFirstMove() if needed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (twoPlayer == false && computer == false)//check if game type has been selected
                GamePlayerTypeNotSelected();
            else
            {
                startButton.Background = Brushes.Pink;
                ResetBoard();
                isGameStarted = true;

                if (whoseTurn == WhoseTurn.player1_X)
                    gameStatusLabel.Content = "Player 1's Turn";
                else if (whoseTurn == WhoseTurn.player2_O)
                    gameStatusLabel.Content = "Player 2's Turn";
                else if (whoseTurn == WhoseTurn.computer_O)
                    gameStatusLabel.Content = "Computer's Turn";
                if (computerMakesFirstMove)
                    ComputerFirstMove();
            }
            if(computerMakesFirstMove)
                ComputerFirstMove();
        }
#endregion
#region changePlayerTypeButton_Click Method
        /// <summary>
        /// Method that executes when the 'Change Player Type & Reset' button is clicked.
        /// Updates buttons, variables, and labels associated with button.
        /// Calls methods to resets board and stats 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changePlayerTypeButton_Click(object sender, RoutedEventArgs e)
        {
            twoPlayerButton.Background = Brushes.LightGray;
            computerButton.Background = Brushes.LightGray;
            startButton.Background = Brushes.LightGoldenrodYellow;

            twoPlayer = false;
            computer = false;
            isGameStarted = false;
            computerMakesFirstMove = false;
            whoseTurn = WhoseTurn.player1_X;

            player1NumberOfWinsLabel.Content = "0";
            player2OrComputerNumberOfWinsLabel.Content = "0";
            numberOfTiesLabel.Content = "0";
            player2OrComputerWinsLabel.Content = "-";
            gameStatusLabel.Content = "Choose Two Player or Computer";

            clsTicTac.ResetStats();//reset stats used for logic
            ResetBoard();
        }
#endregion
#region spaceClickButton Method
        /// <summary>
        /// Method that executes when any board space clicked.
        /// Checks if 'Computer' or 'Two Player' button is selected. Displays message if needed.
        /// Checks if 'Start A New Game' button has be clicks. Displays message if needed.
        /// Checks if a board space is already filled. Displays message if needed.
        /// Updates buttons, variables, and labels associated with button based on game player type.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spaceClickButton(object sender, RoutedEventArgs e)
        {
            Button MyButton = (Button)sender;//Cast the sender into the button variable

            startButton.Background = Brushes.LightGoldenrodYellow;//visual to show that start button can be clicked again

            if (twoPlayer == false && computer == false)//check if game player type is selected
                GamePlayerTypeNotSelected();

            else if (isGameStarted == false)//check if start button has been clicked
                StartGameButtonNotPressed();

            else if (MyButton.Content.ToString() == "X" || MyButton.Content.ToString() == "O")//check if player chose 
                SpaceAlreadySet();                                                            //a space already selected

            else if (twoPlayer)
            {
                if (whoseTurn == WhoseTurn.player1_X)
                {
                    MyButton.Content = "X";//set space content to X
                    MyButton.Background = Brushes.SlateBlue;//set space color
                    whoseTurn = WhoseTurn.player2_O;//player 2's turn
                    clsTicTac.SetSpaceToX(MyButton.Name);//set board used for logic to X

                    if (clsTicTac.IsWinningMove())//was the move a win
                    {
                        DetermineWhoWon();//determine and set winner variable 
                        DisplayStats();
                        ShowWin();//hight light spaces that consist of the winning spaces
                        whoseTurn = WhoseTurn.player1_X;//player 1 won the round. Player 1 goes first next round
                    }
                    else if (clsTicTac.IsTie())
                    {
                        tieGame = true;
                        DisplayStats();
                        whoseTurn = WhoseTurn.player1_X;//odd number of spaces. Player 1 drew tie so they go first next round
                    }
                    else
                        gameStatusLabel.Content = "Player 2's Turn";
                }

                else if (whoseTurn == WhoseTurn.player2_O)
                {
                    MyButton.Content = "O";//set space to O
                    MyButton.Background = Brushes.SteelBlue;//set space color
                    whoseTurn = WhoseTurn.player1_X;//player 1's turn
                    clsTicTac.SetSpaceToO(MyButton.Name);//set board used for logic to O

                    if (clsTicTac.IsWinningMove())
                    {
                        DetermineWhoWon();// determine and set winner variable
                        DisplayStats();
                        ShowWin();//hight light spaces that consist of the winning spaces
                        whoseTurn = WhoseTurn.player2_O;//player 2 won the round. Player 2 goes first next round
                    }
                    else if (clsTicTac.IsTie())
                    {
                        tieGame = true;
                        DisplayStats();
                        whoseTurn = WhoseTurn.player2_O;//odd number of spaces. Player 2 drew tie so they go first next round
                    }
                    else
                        gameStatusLabel.Content = "Player 1's Turn";
                }
            }

            else if (computer)
            {
                if (whoseTurn == WhoseTurn.player1_X)//player 1's turn. Make space an X
                {
                    clsTicTac.SetSpaceToX(MyButton.Name);
                    MyButton.Content = "X";
                    MyButton.Background = Brushes.SlateBlue;
                    whoseTurn = WhoseTurn.computer_O;

                    if (clsTicTac.IsWinningMove())
                    {
                        DetermineWhoWon();
                        DisplayStats();
                        ShowWin();
                        whoseTurn = WhoseTurn.player1_X;//end of game. Prevents computer from playing and sets for next game
                    }
                    else if (clsTicTac.IsTie())
                    {
                        tieGame = true;
                        DisplayStats();
                        whoseTurn = WhoseTurn.player1_X;
                    }
                    else
                        gameStatusLabel.Content = "Computer's Turn";
                }

                if (whoseTurn == WhoseTurn.computer_O)
                {
                    string spaceToSet = clsTicTac.ComputerChoice();
                    SetButtonContent(spaceToSet);
                    clsTicTac.SetSpaceToO(spaceToSet);
                    whoseTurn = WhoseTurn.player1_X;

                    if (clsTicTac.IsWinningMove())
                    {
                        DetermineWhoWon();
                        DisplayStats();
                        ShowWin();
                        whoseTurn = WhoseTurn.computer_O;
                        computerMakesFirstMove = true;//computer won so will go first next round
                    }
                    else if (clsTicTac.IsTie())
                    {
                        tieGame = true;
                        DisplayStats();
                        whoseTurn = WhoseTurn.computer_O;
                        computerMakesFirstMove = true;//odd number of spaces. Computer drew tie so it will go first next round
                    }
                    else
                        gameStatusLabel.Content = "Player 1's Turn";
                }
            }
        }
#endregion
#region ResetBoard Method
        /// <summary>
        /// Resets button content and color to initial state.
        /// Updates variables associated to game outcome to initial state.
        /// Calls method that resets the board used for logic in clsTicTacToe
        /// </summary>
        private void ResetBoard()
        {
            player1IsTheWinner = false;
            player2OrComputerIsTheWinner = false;
            tieGame = false;

            r0c0.Content = "";
            r0c1.Content = "";
            r0c2.Content = "";
            r1c0.Content = "";
            r1c1.Content = "";
            r1c2.Content = "";
            r2c0.Content = "";
            r2c1.Content = "";
            r2c2.Content = "";

            r0c0.Background = Brushes.Lavender;
            r0c1.Background = Brushes.Lavender;
            r0c2.Background = Brushes.Lavender;
            r1c0.Background = Brushes.Lavender;
            r1c1.Background = Brushes.Lavender;
            r1c2.Background = Brushes.Lavender;
            r2c0.Background = Brushes.Lavender;
            r2c1.Background = Brushes.Lavender;
            r2c2.Background = Brushes.Lavender;

            clsTicTac.ResetSpaces();//reset board used for logic
            clsTicTac.ResetWinner();//reset winner variables used for logic
        }
#endregion
#region DetermineWhoWon Method
        /// <summary>
        /// Determines who won the round.
        /// Updates winner variables 
        /// </summary>
        private void DetermineWhoWon()
        {
            if(clsTicTac.GetIfPlayer1Won())
                player1IsTheWinner = true;
            else if (clsTicTac.GetIfPlayer2OrComputerWon())
                player2OrComputerIsTheWinner = true;
        }
#endregion
#region ComputerFirstMove Method
        /// <summary>
        /// Randomly selects a space and sets computer's move to that space.
        /// Called when computer won the previous round and makes the first move.
        /// Updates buttons, variables, and labels
        /// </summary>
        private void ComputerFirstMove()
        {
            Random randSpace = new Random();
            int randRow = randSpace.Next(0, 3);//pick a random row
            int randColumn = randSpace.Next(0, 3);//pick a random column

            for (int row = 0; row < 3; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    if (randRow == row && randColumn == column)
                    {
                        string spaceToSet = "r" + row + "c" + column;
                        SetButtonContent(spaceToSet);//set the random button to O
                        clsTicTac.SetSpaceToO(spaceToSet);//set board used for logic to O
                        DisplayStats();
                        whoseTurn = WhoseTurn.player1_X;
                        computerMakesFirstMove = false;//computer made first move. Set to initial value
                    }
                }
            }
        }
#endregion
#region SetButtonContent Method
        /// <summary>
        /// Called when game type is set to computer.
        /// Determines what button to set to O based on computer's choice and updates that button.
        /// </summary>
        /// <param name="spaceToSet"></param>
        private void SetButtonContent(string spaceToSet)
        {
            if (r0c0.Name == spaceToSet)
            {
                r0c0.Content = "O";
                r0c0.Background = Brushes.SteelBlue;
            }
            else if (r0c1.Name == spaceToSet)
            {
                r0c1.Content = "O";
                r0c1.Background = Brushes.SteelBlue;
            }
            else if (r0c2.Name == spaceToSet)
            {
                r0c2.Content = "O";
                r0c2.Background = Brushes.SteelBlue;
            }
            else if (r1c0.Name == spaceToSet)
            {
                r1c0.Content = "O";
                r1c0.Background = Brushes.SteelBlue;
            }
            else if (r1c1.Name == spaceToSet)
            {
                r1c1.Content = "O";
                r1c1.Background = Brushes.SteelBlue;
            }
            else if (r1c2.Name == spaceToSet)
            {
                r1c2.Content = "O";
                r1c2.Background = Brushes.SteelBlue;
            }
            else if (r2c0.Name == spaceToSet)
            {
                r2c0.Content = "O";
                r2c0.Background = Brushes.SteelBlue;
            }
            else if (r2c1.Name == spaceToSet)
            {
                r2c1.Content = "O";
                r2c1.Background = Brushes.SteelBlue;
            }
            else if (r2c2.Name == spaceToSet)
            {
                r2c2.Content = "O";
                r2c2.Background = Brushes.SteelBlue;
            }
        }
#endregion
#region DisplayStats Method
        /// <summary>
        /// Updates labels to reflect the winner and current stats
        /// </summary>
        private void DisplayStats()
        {
            player1NumberOfWinsLabel.Content = clsTicTac.GetNumberOfWinsPlayer1();
            player2OrComputerNumberOfWinsLabel.Content = clsTicTac.GetNumberOfWinsPlayer2OrComputer();
            numberOfTiesLabel.Content = clsTicTac.GetNumberOfTies();

            if (player1IsTheWinner)
                gameStatusLabel.Content = "Player 1 is the Winner!";
            else if (player2OrComputerIsTheWinner && twoPlayer)
                gameStatusLabel.Content = "Player 2 is the Winner!";
            else if (player2OrComputerIsTheWinner && computer)
                gameStatusLabel.Content = "Computer is the Winner!";
            else if (tieGame)
                gameStatusLabel.Content = "       Tie Game!";
        }
#endregion
#region ShowWin Method
        /// <summary>
        /// Finds the winning move and highlights the spaces consisting of the winning move
        /// </summary>
        private void ShowWin()
        {
            if(clsTicTac.winningMove == clsTicTacToe.WinningMove.row1)//row1
            {
                r0c0.Background = Brushes.LightYellow;
                r0c1.Background = Brushes.LightYellow;
                r0c2.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.row2)//row2
            {
                r1c0.Background = Brushes.LightYellow;
                r1c1.Background = Brushes.LightYellow;
                r1c2.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.row3)//row3
            {
                r2c0.Background = Brushes.LightYellow;
                r2c1.Background = Brushes.LightYellow;
                r2c2.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.col1)//col1
            {
                r0c0.Background = Brushes.LightYellow;
                r1c0.Background = Brushes.LightYellow;
                r2c0.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.col2)//col2
            {
                r0c1.Background = Brushes.LightYellow;
                r1c1.Background = Brushes.LightYellow;
                r2c1.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.col3)//col3
            {
                r0c2.Background = Brushes.LightYellow;
                r1c2.Background = Brushes.LightYellow;
                r2c2.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.diag1)//diag1
            {
                r0c0.Background = Brushes.LightYellow;
                r1c1.Background = Brushes.LightYellow;
                r2c2.Background = Brushes.LightYellow;
            }
            else if (clsTicTac.winningMove == clsTicTacToe.WinningMove.diag2)//diag2
            {
                r0c2.Background = Brushes.LightYellow;
                r1c1.Background = Brushes.LightYellow;
                r2c0.Background = Brushes.LightYellow;
            }
        }
#endregion
#region Message Box Methods
        /// <summary>
        /// Displays a message box telling the user to press the 'Start A New Game' button before playing.
        /// </summary>
        private void StartGameButtonNotPressed()
        {
            MessageBoxResult result = MessageBox.Show("Press the 'Start Game' button to begin playing the game",
                "Press Start Game");
        }
        /// <summary>
        /// Displays a message box telling the user to select a game type.
        /// </summary>
        private void GamePlayerTypeNotSelected()
        {
            MessageBoxResult result = MessageBox.Show("Choose 'Two Player' or 'Computer'", "Player Type");
        }
        /// <summary>
        /// Displays a message box telling the user that only one game type can be selected at a time.
        /// If a new game type is desired to click the 'Change Game Type & Reset' button.
        /// </summary>
        private void GamePlayerTypeAlreadySelected()
        {
            MessageBoxResult result = MessageBox.Show("Player type is already selected.\n" +
                "Press the 'Change Player Type & Reset' button then select player type.\n" +
                "This will reset all stats", "Player Type Already Selected");
        }
        /// <summary>
        /// Displays a message box telling the user that the space they selected is has already been selected.
        /// </summary>
        private void SpaceAlreadySet()
        {
            MessageBoxResult result = MessageBox.Show("Space has already been field. Choose a different space", "Space Filled");
        }
        /// <summary>
        /// Displays the rules of the game.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rulesButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("-Player 1 always goes first on the first round\n" +
                "-Winner of the previous round starts first on next round\n" +
                "-A space that has already been selected cannot be changed\n" +
                "-If a tie occurs, the winner of the previous round goes first", "Rules");
        }
#endregion
    }
}