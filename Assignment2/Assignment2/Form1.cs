﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Assignment2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Records how many times the dice is rolled.
        /// </summary>
        int numOfRolls = 0;
        /// <summary>
        /// Records how many rounds the user lost.
        /// </summary>
        int losses = 0;
        /// <summary>
        /// Records how many rounds the user won.
        /// </summary>
        int wins = 0; 
        /// <summary>
        /// Holds the value that the random number generator chose.
        /// </summary>
        int cpuChoice = 0;
        /// <summary>
        /// Holds the value that the user chose.
        /// </summary>
        int playerChoice = 0;

#region Variables to record frequency of each face that is selected by the random number generator.
        /// <summary>
        /// Variables to record frequency of each face that is selected by the random number generator.
        /// </summary>
        int freq1 = 0;
        int freq2 = 0;
        int freq3 = 0;
        int freq4 = 0;
        int freq5 = 0;
        int freq6 = 0;
#endregion

#region Variables to record the number of times a particular face is chosen by the user.
        /// <summary>
        /// Variables to record the number of times a particular face is chosen by the user.
        /// </summary>
        int guessed1 = 0;//for guesses
        int guessed2 = 0;
        int guessed3 = 0;
        int guessed4 = 0;
        int guessed5 = 0;
        int guessed6 = 0;
#endregion

#region Roll click method
        /// <summary>
        /// Simulates the dice being rolled, displays message of the outcome of the round,
        /// and updates all data after each dice roll simulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRoll_Click(object sender, EventArgs e)
        {
            Random randomNum = new Random();
            int dieRandomNum = randomNum.Next(1, 7);//holds the value that the random number generator chose
            cpuChoice = dieRandomNum;

            Int32.TryParse(textBox1.Text, out playerChoice);//Extracts the number from the text-box.

            if (playerChoice > 6 || playerChoice < 1)//if invalid input
                label1.Text = "Invalid Input. \nPlease enter an integer between 1-6";

            if (playerChoice <= 6 && playerChoice >= 1)//if valid input
            {
                diceImage.SizeMode = PictureBoxSizeMode.StretchImage;

                for (int i = 1; i < 8; i++)
                {
                    Random randImage = new Random();//Used to visually simulate the randomness of a dice rolling.
                    int randDieImage = randImage.Next(1, 7);

                    diceImage.Image = Image.FromFile("die" + randDieImage.ToString() + ".gif");
                    diceImage.Refresh();
                    Thread.Sleep(150);
                }
                //So the image will reflect that of the random number chosen.
                diceImage.Image = Image.FromFile("die" + dieRandomNum.ToString() + ".gif");

                if (dieRandomNum != playerChoice)//if the user did not guess the same as the CPU
                {
                    label1.Text = "You guessed " + playerChoice + "\nThe dice rolled a " + dieRandomNum + "\n\nYou lost!";
                    countNumOfLosses();
                }

                if (dieRandomNum == playerChoice)//if the user guessed the same as the CPU
                {
                    label1.Text = "You guessed " + playerChoice + "\nThe dice rolled a " + dieRandomNum + "\n\nYou won!";
                    countNumOfWins();
                }

                playsLabel.Text = (1 + countNumOfRolls()).ToString();
                wonLabel.Text = wins.ToString();
                lostLabel.Text = losses.ToString();

                updateFrequency();
                updateNumberOfTimesGuessed();
                updatePercentage();
            }
        }
        #endregion

#region Count methods to record the number of rolls, wins, and losses for the user.
        private int countNumOfRolls()
        {
            return numOfRolls++;
        }

        private int countNumOfWins()
        {
            return wins++;
        }

        private int countNumOfLosses()
        {
            return losses++;
        }
#endregion
#region Update methods to update frequency, percentage, and number of times guessed.
        /// <summary>
        /// Updates the frequency that the CPU chose a particular face.
        /// </summary>
        private void updateFrequency()
        {
            if (cpuChoice == 1)
                label13.Text = (++freq1).ToString();

            else if (cpuChoice == 2)
                label14.Text = (++freq2).ToString();

            else if (cpuChoice == 3)
                label15.Text = (++freq3).ToString();

            else if (cpuChoice == 4)
                label16.Text = (++freq4).ToString();

            else if (cpuChoice == 5)
                label17.Text = (++freq5).ToString();

            else
                label18.Text = (++freq6).ToString();
        }
        /// <summary>
        /// Updates the number of times the user chose a particular face as their guess.
        /// </summary>
        private void updateNumberOfTimesGuessed()
        {
            if (playerChoice == 1)
                label25.Text = (++guessed1).ToString();

            else if (playerChoice == 2)
                label26.Text = (++guessed2).ToString();

            else if (playerChoice == 3)
                label27.Text = (++guessed3).ToString();

            else if (playerChoice == 4)
                label28.Text = (++guessed4).ToString();

            else if (playerChoice == 5)
                label29.Text = (++guessed5).ToString();

            else
                label30.Text = (++guessed6).ToString();
        }
        /// <summary>
        /// Updates the percentage of times, in respect to number of rolls, 
        /// the CPU chose a particular face.
        /// </summary>
        private void updatePercentage()
        {
            label19.Text = String.Format("{0:P2}", ((double)freq1 / numOfRolls));
            label20.Text = String.Format("{0:P2}", ((double)freq2 / numOfRolls));
            label21.Text = String.Format("{0:P2}", ((double)freq3 / numOfRolls));
            label22.Text = String.Format("{0:P2}", ((double)freq4 / numOfRolls));
            label23.Text = String.Format("{0:P2}", ((double)freq5 / numOfRolls));
            label24.Text = String.Format("{0:P2}", ((double)freq6 / numOfRolls));
        }
#endregion
#region Method to reset game back to initial state.
        /// <summary>
        /// Resets the games to its initial state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdReset_Click(object sender, EventArgs e)
        {
            numOfRolls = 0; 
            losses = 0; 
            wins = 0; 
            cpuChoice = 0;
            playerChoice = 0;

            playsLabel.Text = numOfRolls.ToString();
            wonLabel.Text = wins.ToString();
            lostLabel.Text = losses.ToString();

            freq1 = 0;
            freq2 = 0;
            freq3 = 0;
            freq4 = 0;
            freq5 = 0;
            freq6 = 0;

            guessed1 = 0;
            guessed2 = 0;
            guessed3 = 0;
            guessed4 = 0;
            guessed5 = 0;
            guessed6 = 0;

            label13.Text = freq1.ToString();
            label14.Text = freq2.ToString();
            label15.Text = freq3.ToString();
            label16.Text = freq4.ToString();
            label17.Text = freq5.ToString();
            label18.Text = freq6.ToString();
            label19.Text = String.Format("{0:P2}", 0);
            label20.Text = String.Format("{0:P2}", 0);
            label21.Text = String.Format("{0:P2}", 0);
            label22.Text = String.Format("{0:P2}", 0);
            label23.Text = String.Format("{0:P2}", 0);
            label24.Text = String.Format("{0:P2}", 0);
            label25.Text = guessed1.ToString();
            label26.Text = guessed2.ToString();
            label27.Text = guessed3.ToString();
            label28.Text = guessed4.ToString();
            label29.Text = guessed5.ToString();
            label30.Text = guessed6.ToString();
        }
#endregion
    }
}