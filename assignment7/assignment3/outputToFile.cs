﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Xml;

namespace assignment3
{
    /// <summary>
    /// Class that handles all logic for writing to a file on a separate thread
    /// </summary>
    public class outputToFile
    {
        /// <summary>
        /// Used to read data from a file
        /// </summary>
        StreamReader MyStream;

        /// <summary>
        /// Checks to see a file already exist
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>boolean value if a file exist</returns>
        public bool doesFileExist(string fileName)
        {
            try
            {
                string file = @"C:\Users\Public\" + fileName;

                if (File.Exists(file))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Checks to see if a file for a specific file path exist
        /// </summary>
        /// <param name="pathName"></param>
        /// <returns></returns>
        public bool doesPathExist(string pathName)
        {
            try
            {
                if (File.Exists(pathName))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Creates a file based of user file name and writes what is in the scores textbox to that file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="scoresTextBoxData"></param>
        public void createAndWriteFile(string fileName, string scoresTextBoxDisplayData)
        {
            try
            {
                string file = @"C:\Users\Public\" + fileName;

                //Create the stream writer
                using (StreamWriter MyWriter = new StreamWriter(file))
                {
                    //Write the data to the file
                    MyWriter.Write(scoresTextBoxDisplayData);
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Read all of the data from the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private string readAllDataFromFile(string fileName)
        {
            string allFileData;

            //Create a stream reader and pass in the file
            using (StreamReader FileRead = new StreamReader(fileName))
            {
                //Read to the end of the file
                allFileData = FileRead.ReadToEnd();

                return allFileData;

                //Don't need to call "Close" because we use the "using" statement
                //The "using" statement is the same as a try/catch block and putting the "FileRead.Dispose" method call in the "finally" statment
                //FileRead.Close();
            }
        }
        /// <summary>
        /// Open a file for reading.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public string openFile(string fileName)
        {
            try
            {
                string fileData = string.Empty;
                //Create the stream reader to get the text
                MyStream = File.OpenText(fileName);
                fileData = readAllDataFromFile(fileName);

                return fileData;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
            
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (System.Exception ex)
            {
                System.IO.File.AppendAllText(@"C:\Error.txt", Environment.NewLine + "HandleError Exception: " + ex.Message);
            }
        }
    }
}
