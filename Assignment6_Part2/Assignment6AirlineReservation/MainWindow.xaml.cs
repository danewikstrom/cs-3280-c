﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Assignment6AirlineReservation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        wndAddPassenger wndAddPass;

        /// <summary>
        /// Flight object of the selected flight
        /// </summary>
        public Flight selectedFlight;
        /// <summary>
        /// Passenger object of the selected passenger
        /// </summary>
        public Passenger selectedPassenger;
        /// <summary>
        /// List of passengers for a particular flight
        /// </summary>
        public List<Passenger> passengerList;
        /// <summary>
        /// Holds the selected flight ID to be passed to other methods in other classes
        /// </summary>
        public string selectedFlightID;
        /// <summary>
        /// Holds a boolean value if a flight has been selected
        /// </summary>
        bool flightIsSelected;
        /// <summary>
        /// Holds a boolean value if a passenger has been selected
        /// </summary>
        bool passengerIsSelected;
        /// <summary>
        /// Hold a boolean value if a seat has been selected for a new passenger
        /// </summary>
        bool seatSelected;
        /// <summary>
        /// Holds a boolean value if a user wants to change a seat
        /// </summary>
        bool changingSeat;
        /// <summary>
        /// Holds a boolean value if a user is adding a new passenger
        /// </summary>
        bool addingPassenger;

        public MainWindow()
        {
            InitializeComponent();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            selectedFlight = new Flight();
            selectedPassenger = new Passenger();
            flightIsSelected = false;
            passengerIsSelected = false;
            seatSelected = false;
            changingSeat = false;
            addingPassenger = false;
            selectedFlightID = string.Empty;
            loadFlights();
        }
        /// <summary>
        /// Executes when a user selects a flight from the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbChooseFlight_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                chooseFlight();
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when a user selects a passenger
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbChoosePassenger_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbChoosePassenger.SelectedIndex != -1)//avoids object reference not set to an instance of an object error.
                    choosePassenger();                   //occurs when the passenger combo box is cleared and the combo box selection change event occurs.
                                                         //if the selected index is a -1 then we know that the choosePassenger method should not be called.
            }

            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when a user clicks the add passenger button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAddPassenger_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                addPassenger();
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when the user clicks on a seat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void seatLabelClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Label seatLabel = (Label)sender;
                //If user clicks a taken seat while a flight is selected then they want to select that passenger
                if (flightIsSelected && cbChooseFlight.IsEnabled == true && seatLabel.Background == Brushes.Red)
                    changePassenger(seatLabel);
                else if (addingPassenger)//if user has clicked the add passenger and saved the new passenger                            
                    chooseSeat(sender);                               
                else if (changingSeat)//if user has clicked the change seat button
                    changeSeat(sender);
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when the user clicks the delete passenger button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDeletePassenger_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                deletePassenger();
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Executes when the user clicks the change seat button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdChangeSeat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                changeSeatButtonClick();
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen to load flights into the combo box
        /// </summary>
        private void loadFlights()
        {
            try
            {
                List<Flight> flList = new List<Flight>();
                Flight flight = new Flight();
                flList = flight.getFlights();//load flights to the list of Flight objects

                cbChooseFlight.Items.Clear();//must clear combo box so it doesn't just get added to

                for (int i = 0; i < flight.numReturnValuesFlight; i++)//add each flight to the combo box
                    cbChooseFlight.Items.Add(flList[i]);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen to load passengers into the combo box
        /// </summary>
        private void loadPassengers()
        {
            try
            {
                passengerList = selectedPassenger.getPassengers(selectedFlightID);//load the passenger list with all passengers objects for a particular flight
                
                if (passengerIsSelected)//if a passenger has already been selected, unselect the selected passenger from the combo box
                    cbChoosePassenger.SelectedIndex = -1;
                
                cbChoosePassenger.Items.Clear();//must clear combo box so it doesn't just get added to

                foreach (Passenger aPassenger in passengerList)//add each passenger to the combo box
                    cbChoosePassenger.Items.Add(aPassenger);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user selects a flight
        /// </summary>
        private void chooseFlight()
        {
            try
            {
                if (flightIsSelected)//if a new flight is selected
                {
                    selectedFlight = (Flight)cbChooseFlight.SelectedItem;
                    selectedFlightID = selectedFlight.flightID.ToString();
                    loadPassengers();//load passengers for the new selected flight to the passenger combo box
                    passengerIsSelected = false;
                    lblPassengersSeatNumber.Content = string.Empty;
                    setAllSeatsToBlue();//reset all seats to blue
                }
                else
                {
                    selectedFlight = (Flight)cbChooseFlight.SelectedItem;
                    selectedFlightID = selectedFlight.flightID.ToString();
                    loadPassengers();//load passenger for the selected flight
                    flightIsSelected = true;
                    cbChoosePassenger.IsEnabled = true;
                    gPassengerCommands.IsEnabled = true;
                }

                if (selectedFlight.flightAircraftType == "Boeing 767")
                {
                    CanvasA380.Visibility = Visibility.Hidden;
                    Canvas767.Visibility = Visibility.Visible;
                }
                else if (selectedFlight.flightAircraftType == "Airbus A380")
                {
                    Canvas767.Visibility = Visibility.Hidden;
                    CanvasA380.Visibility = Visibility.Visible;
                }

                setTakenSeatsToRed();//set taken seats to red
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user selects a passenger
        /// </summary>
        private void choosePassenger()
        {
            try
            {
                Passenger passenger = new Passenger();
                passengerIsSelected = true;
                passenger = (Passenger)cbChoosePassenger.SelectedItem;
                selectedPassenger = passenger;
                //Set the seat label
                lblPassengersSeatNumber.Content = passenger.passengerSeat.ToString();

                setSelectedPassengerSeatToGreen(selectedPassenger);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user selects a seat
        /// </summary>
        private void chooseSeat(object sender)
        {
            try
            {
                if (!seatSelected)
                {
                    Label MySeatLabel = (Label)sender;

                    if (MySeatLabel.Background == Brushes.Blue)//selected seat is available
                    {
                        //add the new passenger to the database
                        selectedPassenger.addNewPassengerToDatabase(selectedPassenger.passengerFirstName, selectedPassenger.passengerLastName);
                        //get the new passenger's ID from database
                        selectedPassenger.passengerID = selectedPassenger.getNewPassengerID(selectedPassenger.passengerFirstName, selectedPassenger.passengerLastName);
                        //set selected passenger's seat
                        selectedPassenger.passengerSeat = MySeatLabel.Content.ToString();
                        //add the new passenger to the bridge / link table in the database
                        selectedPassenger.addNewPassengerToLinkTable(selectedFlightID, selectedPassenger.passengerID, selectedPassenger.passengerSeat);
                        //add the new passenger to the combo box
                        cbChoosePassenger.Items.Add(selectedPassenger);
                        //add the new passenger to the passenger list
                        passengerList.Add(selectedPassenger);
                        //set the selected passenger in the combo box to the new passenger
                        cbChoosePassenger.SelectedItem = selectedPassenger;
                        //set seat label to passenger's seat number
                        lblPassengersSeatNumber.Content = selectedPassenger.passengerSeat;
                        //set the selected seat of the new passenger to green
                        MySeatLabel.Background = Brushes.LawnGreen;
                        addingPassenger = false;
                        seatSelected = true;
                        enableWindow();
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user clicks the add passenger button.
        /// </summary>
        private void addPassenger()
        {
            try
            {
                wndAddPass = new wndAddPassenger();
                wndAddPass.ShowDialog();

                if(wndAddPass.newPassengerIsSaved)
                {
                    addingPassenger = true;
                    selectedPassenger = wndAddPass.newPassenger;
                    disableWindow();
                    seatSelected = false;
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Handles events that need to happen when the user clicks the delete passenger button
        /// </summary>
        private void deletePassenger()
        {
            try
            {
                if (flightIsSelected && passengerIsSelected)
                {
                    //delete passenger from the bridge / link table in the database
                    selectedPassenger.deletePassengerFromLinkTable(selectedPassenger.passengerSeat, selectedFlightID);
                    //delete passenger from the database
                    selectedPassenger.deleteAPassenger(selectedPassenger.passengerID);
                    //remove passenger from the combo box
                    cbChoosePassenger.Items.Remove(selectedPassenger);
                    lblPassengersSeatNumber.Content = string.Empty;
                    setAllSeatsToBlue();
                    setTakenSeatsToRed();
                    //unselect the passenger from the combo box
                    cbChoosePassenger.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Deletes a passenger when a red taken seat is clicked
        /// </summary>
        private void changePassenger(Label seatLabel)
        {
            try
            {
                string seatNum = seatLabel.Content.ToString();
                //Get the passenger based of a the selected seat number and current flight ID
                selectedPassenger = selectedPassenger.getPassengerFromSeatClick(seatNum, selectedFlightID);
                //set the selected passenger's passenger ID
                selectedPassenger.passengerID = selectedPassenger.getNewPassengerID(selectedPassenger.passengerFirstName, selectedPassenger.passengerLastName);
                setAllSeatsToBlue();
                setTakenSeatsToRed();
                setSelectedPassengerSeatToGreen(selectedPassenger);
                lblPassengersSeatNumber.Content = seatNum;
                //set the selected item in combo box to the selected passenger
                cbChoosePassenger.SelectedItem = selectedPassenger;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Reset all seats to blue. Used when user selects another flight
        /// </summary>
        private void setAllSeatsToBlue()
        {
            if (selectedFlight.flightAircraftType == "Boeing 767")
            {
                foreach (Label MySeatLabel in c767_Seats.Children)
                {
                    MySeatLabel.Background = Brushes.Blue;
                }
            }
            if (selectedFlight.flightAircraftType == "Airbus A380")
            {
                foreach (Label MySeatLabel in cA380_Seats.Children)
                {
                    MySeatLabel.Background = Brushes.Blue;
                }
            }

        }
        /// <summary>
        /// Sets all taken seats to red
        /// </summary>
        private void setTakenSeatsToRed()
        {
            try
            {
                List<string> listOfTakenSeats = selectedFlight.getTakenSeatsForFlight(selectedFlightID);

                if (selectedFlight.flightAircraftType == "Boeing 767")
                {
                    //Need to find the selected seat in the panel.  Loop through each label in the panel.
                    foreach (Label MySeatLabel in c767_Seats.Children)
                    {
                        //Check to see if the passenger's seat matches the label
                        if (listOfTakenSeats.Contains(MySeatLabel.Content.ToString()))
                        {
                            MySeatLabel.Background = Brushes.Red;
                        }
                    }
                }
                if (selectedFlight.flightAircraftType == "Airbus A380")
                {
                    //Need to find the selected seat in the panel.  Loop through each label in the panel.
                    foreach (Label MySeatLabel in cA380_Seats.Children)
                    {
                        //Check to see if the passenger's seat matches the label
                        if (listOfTakenSeats.Contains(MySeatLabel.Content.ToString()))
                        {
                            MySeatLabel.Background = Brushes.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
            
        }
        /// <summary>
        /// Sets the selected passengers seat to green
        /// </summary>
        private void setSelectedPassengerSeatToGreen(Passenger selectedPassenger)
        {
            try
            {
                if (selectedFlight.flightAircraftType == "Boeing 767")
                {
                    //Need to find the selected seat.  Loop through each label.
                    foreach (Label MySeatLabel in c767_Seats.Children)
                    {
                        if (MySeatLabel.Background == Brushes.Blue)//available seat so don't worry about
                            continue;
                        //if already green turn blue so previously selected passengers seat doesn't stay green
                        else if (MySeatLabel.Background == Brushes.LawnGreen)
                            MySeatLabel.Background = Brushes.Red;

                        //Check to see if the passenger's seat matches the label
                        if (MySeatLabel.Content.ToString() == selectedPassenger.passengerSeat)
                            MySeatLabel.Background = Brushes.LawnGreen;
                    }
                }
                if (selectedFlight.flightAircraftType == "Airbus A380")
                {
                    //Need to find the selected seat.  Loop through each label.
                    foreach (Label MySeatLabel in cA380_Seats.Children)
                    {
                        if (MySeatLabel.Background == Brushes.Blue)//available seat so don't worry
                            continue;
                        //if already green turn blue so previously selected passengers seat doesn't stay green
                        else if (MySeatLabel.Background == Brushes.LawnGreen)
                            MySeatLabel.Background = Brushes.Red;

                        //Check to see if the passenger's seat matches the label
                        if (MySeatLabel.Content.ToString() == selectedPassenger.passengerSeat)
                            MySeatLabel.Background = Brushes.LawnGreen;
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Disables window so signal to user to selected a new seat
        /// </summary>
        private void changeSeatButtonClick()
        {
            try
            {
                changingSeat = true;
                disableWindow();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
            
        }
        /// <summary>
        /// Allows the user to click on an available seat to assign it to a passenger
        /// </summary>
        /// <summary>
        /// Changes the passengers seat based off the selected available seat
        /// </summary>
        private void changeSeat(object sender)
        {
            try
            {
                Label MySeatLabel = (Label)sender;

                if(MySeatLabel.Background == Brushes.Blue)
                {
                    selectedPassenger = (Passenger)cbChoosePassenger.SelectedItem;
                    selectedPassenger.passengerSeat = MySeatLabel.Content.ToString();
                    selectedPassenger.setPassengerSeatNumber(selectedPassenger.passengerID, selectedPassenger.passengerSeat, selectedFlightID);
                    lblPassengersSeatNumber.Content = selectedPassenger.passengerSeat;
                    setAllSeatsToBlue();
                    setTakenSeatsToRed();
                    setSelectedPassengerSeatToGreen(selectedPassenger);
                    enableWindow();
                    changingSeat = false;
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Enables the window for use
        /// </summary>
        private void enableWindow()
        {
            try
            {
                cbChooseFlight.IsEnabled = true;
                cbChoosePassenger.IsEnabled = true;
                cmdAddPassenger.IsEnabled = true;
                cmdDeletePassenger.IsEnabled = true;
                cmdChangeSeat.IsEnabled = true;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Disables the window for use
        /// </summary>
        private void disableWindow()
        {
            try
            {
                cbChooseFlight.IsEnabled = false;
                cbChoosePassenger.IsEnabled = false;
                cmdAddPassenger.IsEnabled = false;
                cmdDeletePassenger.IsEnabled = false;
                cmdChangeSeat.IsEnabled = false;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (System.Exception ex)
            {
                System.IO.File.AppendAllText(@"C:\Error.txt", Environment.NewLine + "HandleError Exception: " + ex.Message);
            }
        }
    }
}
