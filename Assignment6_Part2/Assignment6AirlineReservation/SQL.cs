﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
using System.Windows;


namespace Assignment6AirlineReservation
{
    /// <summary>
    /// Class to hold all SQL statements
    /// </summary>
    public static class SQL
    {
        /// <summary>
        /// Returns a SQL statement to display flight numbers in flight combo box
        /// </summary>
        /// <returns>string SQL statement</returns>
        public static string returnAllFlights()
        {
            try
            {
                string SQL_Statement = "SELECT Flight_ID, Flight_Number, Aircraft_Type FROM FLIGHT";

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to display passengers for a particular flight in passenger combo box
        /// </summary>
        /// <param name="selectedFlight"></param>
        /// <returns>string SQL statement</returns>
        public static string returnAllPassengersFromFlight(string selectedFlightID)
        {
            try
            {
               string SQL_Statement = "SELECT PASSENGER.Passenger_ID, First_Name, Last_Name, Seat_Number " +
                                      "FROM FLIGHT_PASSENGER_LINK, FLIGHT, PASSENGER " +
                                      "WHERE FLIGHT.FLIGHT_ID = FLIGHT_PASSENGER_LINK.FLIGHT_ID AND " +
                                      "FLIGHT_PASSENGER_LINK.PASSENGER_ID = PASSENGER.PASSENGER_ID AND " +
                                      "FLIGHT.FLIGHT_ID = " + selectedFlightID;

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to get all taken seats from a particular flight
        /// </summary>
        /// <param name="selectedFlight"></param>
        /// <returns></returns>
        public static string returnAllTakenSeatsFromFlight(string selectedFlight)
        {
            try
            {
                string SQL_Statement = "SELECT Seat_Number " +
                                       "FROM Flight_Passenger_link " +
                                       "WHERE Flight_ID = " + selectedFlight;

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to update a passenger's seat number
        /// </summary>
        /// <returns></returns>
        public static string updatingSeatNumber(string passengerID, string selectedSeatNumber, string flightID)
        {
            try
            {
                string SQL_Statement = "UPDATE FLIGHT_PASSENGER_LINK " +
                                       "SET Seat_Number = '" + selectedSeatNumber + "' " +
                                       "WHERE FLIGHT_ID = " + flightID + " AND PASSENGER_ID = " + passengerID + "";
                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns SQL statement to insert a new passenger into the link table
        /// </summary>
        /// <param name="flightID"></param>
        /// <param name="passengerID"></param>
        /// <param name="seatNum"></param>
        /// <returns></returns>
        public static string insertPassengerIntoLinkTable(string flightID, string passengerID, string seatNum)
        {
            try
            {
                string SQL_Statement = "INSERT INTO Flight_Passenger_Link(Flight_ID, Passenger_ID, Seat_Number) " +
                                       "VALUES(" + flightID + ", " + passengerID + ", " + seatNum + ")";

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns SQL statement to delete a passenger from the link table
        /// </summary>
        /// <param name="seatNum"></param>
        /// <param name="flightID"></param>
        /// <returns></returns>
        public static string deleteTheLink(string seatNum, string flightID)
        {
            try
            {
                string SQL_Statement = "Delete FROM FLIGHT_PASSENGER_LINK " +
                                       "WHERE FLIGHT_ID = " + flightID + " AND " +
                                       "Seat_Number = '" + seatNum + "'";

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to delete a passenger from the database
        /// </summary>
        /// <param name="passengerID"></param>
        /// <returns></returns>
        public static string deletePassenger(string passengerID)
        {
            try
            {
                string SQL_Statement = "Delete FROM PASSENGER " +
                                       "WHERE PASSENGER_ID = " + passengerID;

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to insert a new passenger into the database
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <returns></returns>
        public static string insertPassenger(string FirstName, string LastName)
        {
            try
            {
                string SQL_Statement = "INSERT INTO PASSENGER(First_Name, Last_Name) VALUES('" + FirstName + "', '" + LastName + "')";

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to get a particular passenger's passengerID
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <returns></returns>
        public static string getNewPassengersID(string FirstName, string LastName)
        {
            try
            {
                string SQL_Statement = "SELECT Passenger_ID FROM Passenger WHERE First_Name = '" + FirstName + "' AND Last_Name = '" + LastName +"'";

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Returns a SQL statement to get a passenger based of a seat that was clicked
        /// </summary>
        /// <param name="seatNum"></param>
        /// <param name="flightID"></param>
        /// <returns></returns>
        public static string getPassengerFromSeatClick(string seatNum, string flightID)
        {
            try
            {
                string SQL_Statement = "SELECT P.First_Name, P.Last_Name, P.Passenger_ID, FPL.Seat_Number " +
                                       "FROM Flight_Passenger_Link FPL " +
                                       "INNER JOIN Passenger P " +
                                       "ON P.Passenger_ID = FPL.Passenger_ID " +
                                       "WHERE FPL.Flight_ID = " + flightID +
                                       " AND FPL.Seat_Number = '" + seatNum + "'";

                return SQL_Statement;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private static void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
