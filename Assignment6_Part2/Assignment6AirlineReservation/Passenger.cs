﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
using System.Data.OleDb;
using System.Windows;

namespace Assignment6AirlineReservation
{
    /// <summary>
    /// Holds data for a passenger
    /// </summary>
    public class Passenger
    {
        /// <summary>
        /// Passenger objects passenger ID
        /// </summary>
        public string passengerID;
        /// <summary>
        /// Passenger objects passenger first name
        /// </summary>
        public string passengerFirstName;
        /// <summary>
        /// Passenger objects passenger last name
        /// </summary>
        public string passengerLastName;
        /// <summary>
        /// Passenger objects passenger flight ID
        /// </summary>
        public string passengerFlightID;
        /// <summary>
        /// Passenger objects passenger seat
        /// </summary>
        public string passengerSeat;
        /// <summary>
        /// Holds number of returned values 
        /// </summary>
        public int numReturnValuesPassenger;

        public Passenger()
        {
            passengerID = string.Empty;
            passengerFirstName = string.Empty;
            passengerLastName = string.Empty;
            passengerFlightID = string.Empty;
            passengerSeat = string.Empty;
            numReturnValuesPassenger = 0;
        }

        /// <summary>
        /// Called to load passengers into the choose passenger combo box
        /// </summary>
        /// <returns>List of passenger objects</returns>
        public List<Passenger> getPassengers(string flightID)
        {
            try
            {
                clsDataAccess dbPassengers = new clsDataAccess();
                List<Passenger> passengerList = new List<Passenger>();
                DataSet dsPassenger = new DataSet();//Holds the return value

                string SQLPassengers; //Holds an SQL Statement

                //Get the SQL statement to extract the passengers
                SQLPassengers = SQL.returnAllPassengersFromFlight(flightID);

                //Extract the passengers and put them into the DataSet
                dsPassenger = dbPassengers.ExecuteSQLStatement(SQLPassengers, ref numReturnValuesPassenger);

                //Loop through the data and create passenger classes
                for (int i = 0; i < numReturnValuesPassenger; i++)
                {
                    Passenger aPassenger = new Passenger();

                    aPassenger.passengerID = dsPassenger.Tables[0].Rows[i][0].ToString();
                    aPassenger.passengerFirstName = dsPassenger.Tables[0].Rows[i]["First_Name"].ToString();
                    aPassenger.passengerLastName = dsPassenger.Tables[0].Rows[i]["Last_Name"].ToString();
                    aPassenger.passengerSeat = dsPassenger.Tables[0].Rows[i][3].ToString();
                    passengerList.Add(aPassenger);
                }
                return passengerList;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }

        }
        /// <summary>
        /// Inserts a new passenger into the database
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        public void addNewPassengerToDatabase(string firstName, string lastName)
        {
            try
            {
                clsDataAccess dbaddPassenger = new clsDataAccess();
                int numRowsadded = 0;//For testing. Ensure that only one row is added to database

                string SQLaddPassenger; //Holds an SQL Statement

                //Get the SQL statement to insert a new passenger into the database
                SQLaddPassenger = SQL.insertPassenger(firstName, lastName);//insert first name and last name into the database

                //Extract the passengers and put them into the DataSet
                numRowsadded = dbaddPassenger.ExecuteNonQuery(SQLaddPassenger);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Inserts a new passenger into the link table
        /// </summary>
        /// <param name="flightID"></param>
        /// <param name="passengerID"></param>
        /// <param name="seatNum"></param>
        public void addNewPassengerToLinkTable(string flightID, string passengerID, string seatNum)
        {
            try
            {
                clsDataAccess dbaddPassenger = new clsDataAccess();
                int numRowsadded = 0;//For testing. Ensure that only one row is added to database

                string SQLaddPassenger; //Holds an SQL Statement

                //Get the SQL statement to insert a new passenger into the database
                SQLaddPassenger = SQL.insertPassengerIntoLinkTable(flightID, passengerID, seatNum);

                //Extract the passengers and put them into the DataSet
                numRowsadded = dbaddPassenger.ExecuteNonQuery(SQLaddPassenger);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }

        }
        /// <summary>
        /// Returns the passenger ID for the new passenger that was added to the database
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public string getNewPassengerID(string firstName, string lastName)
        {
            try
            {
                clsDataAccess dbPassengersID = new clsDataAccess();
                string passengerID;//holds returned passenger ID
                string SQLPassengersID; //Holds an SQL Statement

                //Get the SQL statement to extract the passengers ID
                SQLPassengersID = SQL.getNewPassengersID(firstName, lastName);

                //Extract the passengers and put them into the DataSet
                passengerID = dbPassengersID.ExecuteScalarSQL(SQLPassengersID);

                return passengerID;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Sets the new passenger seat to the user's chosen seat number
        /// </summary>
        /// <param name="passengerID"></param>
        /// <param name="selectedSeatNumber"></param>
        /// <param name="flightID"></param>
        public void setPassengerSeatNumber(string passengerID, string selectedSeatNumber, string flightID)
        {
            try
            {
                clsDataAccess dbupdateSeat = new clsDataAccess();
                int numRowsadded = 0;//For testing. Ensure that zero rows are added to the database

                string SQLupdateSeat; //Holds an SQL Statement

                //Get the SQL statement to insert a new passenger into the database
                SQLupdateSeat = SQL.updatingSeatNumber(passengerID, selectedSeatNumber, flightID);

                //Extract the passengers and put them into the DataSet
                numRowsadded = dbupdateSeat.ExecuteNonQuery(SQLupdateSeat);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Deletes a passenger from the database
        /// </summary>
        /// <param name="PassengerID"></param>
        public void deleteAPassenger(string PassengerID)
        {
            try
            {
                clsDataAccess dbdeletePassenger = new clsDataAccess();
                int numRowsadded = 0;//For testing. Ensure that zero rows are added to the database

                string SQLdeletePassenger; //Holds an SQL Statement

                //Get the SQL statement to insert a new passenger into the database
                SQLdeletePassenger = SQL.deletePassenger(passengerID);

                //Extract the passengers and put them into the DataSet
                numRowsadded = dbdeletePassenger.ExecuteNonQuery(SQLdeletePassenger);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Deletes a passenger from the link table
        /// </summary>
        /// <param name="seatNum"></param>
        /// <param name="flightID"></param>
        public void deletePassengerFromLinkTable(string seatNum, string flightID)
        {
            try
            {
                clsDataAccess dbdeletePassenger = new clsDataAccess();
                int numRowsadded = 0;//For testing. Ensure that zero rows are added to the database
                string SQLdeletePassenger; //Holds an SQL Statement

                //Get the SQL statement to insert a new passenger into the database
                SQLdeletePassenger = SQL.deleteTheLink(seatNum, flightID);

                //Extract the passengers and put them into the DataSet
                numRowsadded =  dbdeletePassenger.ExecuteNonQuery(SQLdeletePassenger);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Get the passenger from a seat click
        /// </summary>
        /// <param name="seatNum"></param>
        /// <param name="flightID"></param>
        /// <returns></returns>
        public Passenger getPassengerFromSeatClick(string seatNum, string flightID)
        {
            try
            {
                clsDataAccess dbPassengers = new clsDataAccess();
                Passenger aPassenger = new Passenger();
                DataSet dsPassenger = new DataSet();//Holds the return value

                string SQLPassengers; //Holds an SQL Statement

                //Get the SQL statement to extract the passengers ID
                SQLPassengers = SQL.getPassengerFromSeatClick(seatNum, flightID);

                //Extract the passengers and put them into the DataSet
                dsPassenger = dbPassengers.ExecuteSQLStatement(SQLPassengers, ref numReturnValuesPassenger);

                aPassenger.passengerID = dsPassenger.Tables[0].Rows[0][0].ToString();
                aPassenger.passengerFirstName = dsPassenger.Tables[0].Rows[0]["First_Name"].ToString();
                aPassenger.passengerLastName = dsPassenger.Tables[0].Rows[0]["Last_Name"].ToString();
                aPassenger.passengerSeat = dsPassenger.Tables[0].Rows[0][3].ToString();

                return aPassenger;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Override the toString method so that this string is displayed in the combo box.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            try
            {
                return passengerFirstName + " " + passengerLastName;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}

