﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment5
{
    /// <summary>
    /// Stores the User's name and age
    /// </summary>
    public class User
    {
        public string userName;
        public string userAge;

        public User()
        {
            userName = string.Empty;
            userAge = string.Empty;
        }
    }
}
