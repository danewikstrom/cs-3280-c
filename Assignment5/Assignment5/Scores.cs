﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;

namespace Assignment5
{
    /// <summary>
    /// Stuct that holds the data members of a score
    /// </summary>
    public struct aScore
    {
        /// <summary>
        /// holds the number of correct answers for a game
        /// </summary>
        public int numCorrectAnswers;
        /// <summary>
        /// holds the number of incorrect answers for a game
        /// </summary>
        public int numIncorrectAnswers;
        /// <summary>
        /// holds the elapsed time of a particular game in a string
        /// </summary>
        public string elapsedTime;
        /// <summary>
        /// holds the elapsed time of a particular game in a TimeSpan
        /// used when comparing time values in the high scores list
        /// </summary>
        public TimeSpan gameTime;
    }

    /// <summary>
    /// Class that determines if a score should be on the high scores list.
    /// Determines order of the high scores in the high scores list
    /// </summary>
    public static class Scores
    {
        /// <summary>
        /// List of top ten high scores
        /// </summary>
        public static List<aScore> highScoresLists = new List<aScore>();
        /// <summary>
        /// the most current score
        /// </summary>
        public static aScore score;
        /// <summary>
        /// bool value if a particular score in the high score list
        /// </summary>
        public static bool isInHighScoreLists = true;
        /// <summary>
        /// holds the number of games played. used to prolong processing if a score made the high score list
        /// </summary>
        private static int numOfGamesPlayed = 0;
        /// <summary>
        /// Sets values for a score and sends the score to be sorted and inserted into the high scores list
        /// </summary>
        /// <param name="correct"></param>
        /// <param name="incorrect"></param>
        /// <param name="time"></param>
        /// <param name="elapsedTime"></param>
        public static void recordScoreToHighScores(int correct, int incorrect, TimeSpan time, string elapsedTime)
        {
            try
            {
                numOfGamesPlayed++;

                score.numCorrectAnswers = correct;
                score.numIncorrectAnswers = incorrect;
                score.elapsedTime = elapsedTime;
                score.gameTime = time;

                sortHighScores();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// Sorts the high scores list after each game
        /// </summary>
        private static void sortHighScores()
        {
            try
            {
                List<aScore> tempList = new List<aScore>(highScoresLists);
                tempList.Add(score);
                //https://social.msdn.microsoft.com/Forums/vstudio/en-US/990b3d83-dca1-4594-b30e-2ac8fbe9716b/how-to-sort-an-arraylist-that-contains-objects-of-data-types?forum=csharpgeneral
                //https://msdn.microsoft.com/en-us/library/bb534743(v=vs.110).aspx

                highScoresLists = tempList.OrderByDescending(a => a.numCorrectAnswers).ThenBy(b => b.gameTime).Take(10).ToList();

                if (numOfGamesPlayed > 10)
                {
                    isInHighScoreLists = false;
                    didMakeHighScoreList(highScoresLists);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// determines if a score made the high scores list
        /// </summary>
        /// <param name="highScoreList"></param>
        private static void didMakeHighScoreList(List<aScore> highScoreList)
        {
            foreach(aScore HSscore in highScoreList)
            {
                if(HSscore.Equals(score))
                    isInHighScoreLists = true;
            }
        }
        /// <summary>
        /// gives back the number of correct answer for a particular game
        /// </summary>
        /// <param name="index"></param>
        /// <returns>number of correct answers</returns>
        public static string getNumCorrectAnswers(int index)
        {
            try
            {
                return highScoresLists.ElementAt(index).numCorrectAnswers.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// gives back the number of incorrect answer for a particular game
        /// </summary>
        /// <param name="index"></param>
        /// <returns>number of incorrect answers</returns>
        public static string getNumIncorrectAnswers(int index)
        {
            try
            {
                return highScoresLists.ElementAt(index).numIncorrectAnswers.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// gives back the elapsed time for a particular game
        /// </summary>
        /// <param name="index"></param>
        /// <returns>a games elapsed time</returns>
        public static string getGameTime(int index)
        {
            try
            {
                return highScoresLists.ElementAt(index).elapsedTime;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// returns how many scores are in the high scores list
        /// </summary>
        /// <returns></returns>
        public static int getHighScoreListCount()
        {
            try
            {
                return highScoresLists.Count;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
    }
}
