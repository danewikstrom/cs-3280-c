﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Reflection;

namespace Assignment5
{
    /// <summary>
    /// Interaction logic for wndSubtractGame.xaml
    /// </summary>
    public partial class wndSubtractGame : Window
    {
        /// <summary>
        /// instantiation of the SubtractGame class
        /// </summary>
        SubtractGame subtractGame;
        /// <summary>
        /// used to update the time label every second
        /// </summary>
        DispatcherTimer updateTimerLabelEverySecond;
        /// <summary>
        /// used to update the question number label
        /// </summary>
        int numOfQuestion;
        /// <summary>
        /// used to show if a game is complete
        /// </summary>
        public bool gameComplete;

        public wndSubtractGame()
        {
            InitializeComponent();

            updateTimerLabelEverySecond = new DispatcherTimer();
            updateTimerLabelEverySecond.Tick += new EventHandler(updateTimerLabelEverySecond_Tick);
            updateTimerLabelEverySecond.Interval = new TimeSpan(0, 0, 1);
            subtractGame = new SubtractGame();
            Sounds.startSubtractGame();
            numOfQuestion = 1;
            gameComplete = false;
        }
        /// <summary>
        /// processes events that need to occur when the start button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                start();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }

        }
        /// <summary>
        /// processes events that need to occur when the submit button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitAnswerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                submit();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to occur when the end button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void endGameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                end();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// validate that the user's input into the answer text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void answerTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                validateAnswer(sender, e);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }

        }
        /// <summary>
        /// logic for when the start button is pressed
        /// </summary>
        private void start()
        {
            try
            {
                startButton.IsDefault = false;
                startButton.Visibility = Visibility.Collapsed;

                answerTextBox.Visibility = Visibility.Visible;
                answerTextBox.Focus();

                submitAnswerButton.Visibility = Visibility.Visible;
                submitAnswerButton.IsDefault = true;

                subtractGame.timer.startTimer();

                updateTimerLabelEverySecond.Start();

                displayQuestion();
                displayQuestionNumber();

                endGameButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// logic for when the submit button is pressed
        /// </summary>
        private void submit()
        {
            try
            {
                bool isAnswerCorrect;
                int userAnswer;

                Int32.TryParse(answerTextBox.Text, out userAnswer);

                isAnswerCorrect = subtractGame.isAnswerCorrect(userAnswer);

                numOfQuestion++;

                if (string.IsNullOrEmpty(answerTextBox.Text))
                {
                    rightOrWrongLabel.Content = "try!";
                    Sounds.dontDoThat();
                }
                else if (numOfQuestion > 10)
                    sumbitLastQuestion();
                else
                {
                    if (isAnswerCorrect)
                    {
                        rightOrWrongLabel.Content = "correct";
                        Sounds.getCorrectSound();
                    }
                    else
                    {
                        rightOrWrongLabel.Content = "wrong";
                        Sounds.getIncorrectSound();
                    }

                    if (numOfQuestion < 10)
                    {
                        displayQuestionNumber();
                        feedBackLabel.Content = Quotes.getRandomQuote();
                        displayQuestion();
                        answerTextBox.Clear();
                        answerTextBox.Focus();
                    }
                    else if (numOfQuestion == 10)
                    {
                        displayQuestionNumber();
                        answerTextBox.Clear();
                        answerTextBox.Focus();
                        submitAnswerButton.Content = "finish!";
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);

            }
        }
        /// <summary>
        /// logic for when the end button is pressed
        /// </summary>
        private void end()
        {
            try
            {
                Sounds.endGame();
                this.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// logic to validate user answers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void validateAnswer(object sender, KeyEventArgs e)
        {
            try
            {
                if (!((e.Key >= Key.D0 && e.Key <= Key.D9) ||
                                  e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
                {
                    //Allow the user to use the backspace and delete keys
                    if (!(e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Enter))
                    {
                        //No other keys allowed besides numbers, backspace, enter/return, and delete
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays random question to the screen
        /// </summary>
        private void displayQuestion()
        {
            try
            {
                questionLabel.Content = subtractGame.createRandomQuestion();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays the question's number
        /// </summary>
        private void displayQuestionNumber()
        {
            try
            {
                questionNumLabel.Content = "question " + numOfQuestion.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to happen when the last question would be submitted
        /// </summary>
        private void sumbitLastQuestion()
        {
            try
            {
                subtractGame.timer.stopTimer();
                updateTimerLabelEverySecond.Stop();

                subtractGame.recordScore(subtractGame.timer.timeSpan, subtractGame.timer.elapsedTime);
                subtractGame.timer.resetTimer();
                gameComplete = true;

                this.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// update time label
        /// info from https://msdn.microsoft.com/en-us/library/system.windows.threading.dispatchertimer.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateTimerLabelEverySecond_Tick(object sender, EventArgs e)
        {
            try
            {
                timeLabel.Content = subtractGame.timer.elapsedTime;

                // Forcing the CommandManager to raise the RequerySuggested event
                CommandManager.InvalidateRequerySuggested();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}

