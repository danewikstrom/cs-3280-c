﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;

namespace Assignment5
{
    /// <summary>
    /// Class that handles all logic involved with a subtraction game
    /// </summary>
    class SubtractGame : Game
    {
        /// <summary>
        /// Holds the correct answer's value
        /// </summary>
        private int correctAnswer = 0;
        /// <summary>
        /// Holds the questions left hand side operand
        /// </summary>
        private int LHS = 0;
        /// <summary>
        /// Holds the questions right hand side operand
        /// </summary>
        private int RHS = 0;
        /// <summary>
        /// Holds the games score
        /// </summary>
        private aScore score = new aScore();

        public SubtractGame()
        {
            gameChoice = Game.gameType.subtract;
        }

        /// <summary>
        /// Randomly generate left and right hand operands
        /// </summary>
        /// <returns>the question</returns>
        public override string createRandomQuestion()
        {
            try
            {
                string question;

                do
                {
                    Random randomNumber = new Random();
                    LHS = randomNumber.Next(0, 11);
                    RHS = randomNumber.Next(0, 11);
                } while (LHS <= RHS);//LHS must be greater than RHS


                question = LHS.ToString() + " - " + RHS.ToString() + " =";

                return question;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if answer is correct and increments the number of (in)correct answers
        /// </summary>
        /// <param name="userAnswer"></param>
        /// <returns>true or false if answer is correct</returns>
        public override bool isAnswerCorrect(int userAnswer)
        {
            try
            {
                correctAnswer = getCorrectAnswer(LHS, RHS);

                if (userAnswer == correctAnswer)
                {
                    numCorrectAnswers++;
                    return true;
                }
                else
                {
                    numInCorrectAnswers++;
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// called by isAnswerCorrect() to get the correct answer
        /// </summary>
        /// <param name="LHS"></param>
        /// <param name="RHS"></param>
        /// <returns>the correct answer</returns>
        private int getCorrectAnswer(int LHS, int RHS)
        {
            try
            {
                correctAnswer = LHS - RHS;
                return correctAnswer;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Records the score of each game
        /// Sends the score to recordScoreToHighScores to determine if the score is a high score
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="elapsedTime"></param>
        public void recordScore(TimeSpan gameTime, string elapsedTime)
        {
            try
            {
                int correct = score.numCorrectAnswers = numCorrectAnswers;
                int incorrect = score.numIncorrectAnswers = numInCorrectAnswers;

                Scores.recordScoreToHighScores(correct, incorrect, gameTime, elapsedTime);
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
