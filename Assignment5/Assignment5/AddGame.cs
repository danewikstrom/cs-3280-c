﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Assignment5
{
    /// <summary>
    /// Class that handles all logic involved with a addition game
    /// </summary>
    class AddGame : Game
    {
        /// <summary>
        /// Holds the correct answer's value
        /// </summary>
        private int correctAnswer = 0;
        /// <summary>
        /// Holds the questions left hand side operand
        /// </summary>
        private int LHS = 0;
        /// <summary>
        /// Holds the questions right hand side operand
        /// </summary>
        private int RHS = 0;
        /// <summary>
        /// Holds the games score
        /// </summary>
        private aScore score = new aScore();

        public AddGame()
        {
            gameChoice = Game.gameType.add;
        }

        /// <summary>
        /// Randomly generate left and right hand operands
        /// </summary>
        /// <returns>the question</returns>
        public override string createRandomQuestion()
        {
            try
            {
                string question;

                Random randomNumber = new Random();
                LHS = randomNumber.Next(0, 11);
                RHS = randomNumber.Next(0, 11);

                question = LHS.ToString() + " + " + RHS.ToString() + " =";

                return question;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if answer is correct and increments the number of (in)correct answers
        /// </summary>
        /// <param name="userAnswer"></param>
        /// <returns>true or false if answer is correct</returns>
        public override bool isAnswerCorrect(int userAnswer)
        {
            try
            {
                correctAnswer = getCorrectAnswer(LHS, RHS);

                if (userAnswer == correctAnswer)
                {
                    numCorrectAnswers++;
                    return true;
                }
                else
                {
                    numInCorrectAnswers++;
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// called by isAnswerCorrect() to get the correct answer
        /// </summary>
        /// <param name="LHS"></param>
        /// <param name="RHS"></param>
        /// <returns>the correct answer</returns>
        private int getCorrectAnswer(int LHS, int RHS)
        {
            try
            {
                correctAnswer = RHS + LHS;
                return correctAnswer;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Records the score of each game
        /// Sends the score to recordScoreToHighScores to determine if the score is a high score
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="elapsedTime"></param>
        public void recordScore(TimeSpan gameTime, string elapsedTime)
        {
            try
            {
                int correct = score.numCorrectAnswers = numCorrectAnswers;
                int incorrect = score.numIncorrectAnswers = numInCorrectAnswers;

                Scores.recordScoreToHighScores(correct, incorrect, gameTime, elapsedTime);
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
    }
}
