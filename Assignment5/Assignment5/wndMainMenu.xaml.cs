﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using System.Media;

namespace Assignment5
{
    /// <summary>
    /// Interaction logic for wndMainMenu.xaml
    /// </summary>
    public partial class wndMainMenu : Window
    {
        /// <summary>
        /// to play star wars theme when game starts
        /// </summary>
        SoundPlayer starWarsTheme = new SoundPlayer("starwarsTheme.wav");

        public wndMainMenu()
        {
            InitializeComponent();
            
            starWarsTheme.Play();
        }
        /// <summary>
        /// processes events that need to when the user information button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                enterInfomation();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to when the add button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string add = "add";
                playAddGame(add);
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to when the subtract button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subtractButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string add = "subtract";
                playAddGame(add);
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to when the multiply button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void multiplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string add = "multiply";
                playAddGame(add);
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to when the divide button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void divideButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string add = "divide";
                playAddGame(add);
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events that need to when the high score button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scorebutton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                displayHighScores();
            }
            catch (Exception ex)
            {
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes events for when the user enters their information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void enterInfomation()
        {
            try
            {
                wndEnterUserData userDataWindow = new wndEnterUserData();

                userDataWindow.ShowDialog();

                if (userDataWindow.DidSubmit)
                {
                    feedBackLabel.Content = "Choose which type of game you want to play.\n\n" +
                                            "\t      may the force be with you!";
                    addButton.IsEnabled = true;
                    subtractButton.IsEnabled = true;
                    multiplyButton.IsEnabled = true;
                    divideButton.IsEnabled = true;
                    highScoresButton.IsEnabled = true;
                    userDataButton.IsDefault = false;
                    User user = new User();
                                   
                    user.userName = userDataWindow.GetName;
                    user.userAge = userDataWindow.GetAge;

                    nameLabel.Content = user.userName;
                    ageLabel.Content = user.userAge;
                    userDataButton.Background = Brushes.LightSteelBlue;
                    userDataButton.Content = "Change Jedi's Information";
                    userDataWindow.Close();

                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// logic for when the user clicks either the add, subtract, multiply, or divide button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void playAddGame(string gameType)
        {
            try
            {
                starWarsTheme.Stop();

                if(gameType == "add")
                {
                    wndAddGame addGameWindow = new wndAddGame();

                    addGameWindow.ShowDialog();

                    if (addGameWindow.gameComplete)
                        displayHighScores();
                }
                else if (gameType == "subtract")
                {
                    wndSubtractGame subtractGameWindow = new wndSubtractGame();

                    subtractGameWindow.ShowDialog();

                    if (subtractGameWindow.gameComplete)
                        displayHighScores();
                }
                else if (gameType == "multiply")
                {
                    wndMultiplyGame multiplyGameWindow = new wndMultiplyGame();

                    multiplyGameWindow.ShowDialog();

                    if (multiplyGameWindow.gameComplete)
                        displayHighScores();
                }
                else if (gameType == "divide")
                {
                    wndDivideGame divideGameWindow = new wndDivideGame();

                    divideGameWindow.ShowDialog();

                    if (divideGameWindow.gameComplete)
                        displayHighScores();
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// logic for when the user clicks the high scores button
        /// </summary>
        private void displayHighScores()
        {
            try
            {
                wndScores scoresWindow = new wndScores();

                scoresWindow.ShowDialog();

            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
