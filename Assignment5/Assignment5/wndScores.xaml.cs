﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Assignment5
{
    /// <summary>
    /// Interaction logic for wndScores.xaml
    /// </summary>
    public partial class wndScores : Window
    {
        public wndScores()
        {
            InitializeComponent();
            showScores();
        }
        private void mainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mainMenu();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
            
        }

        private void mainMenu()
        {
            try
            {
                Sounds.endGame();
                this.Close();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        public void showScores()
        {
            try
            {
                gameScoreLabel.Content = "you answered " + Scores.score.numCorrectAnswers.ToString() +
                " out of 10 questions correct";

                scoreFeedBack();

                Label[] correctAnswersLabelArray = {cLabel1, cLabel2, cLabel3, cLabel4, cLabel5, cLabel6,
                                                cLabel7, cLabel8, cLabel9, cLabel10};
                Label[] inCorrectAnswersLabelArray = {iLabel1, iLabel2, iLabel3, iLabel4, iLabel5, iLabel6,
                                                iLabel7, iLabel8, iLabel9, iLabel10};
                Label[] gameTimeLabelArray = {gtLabel1, gtLabel2, gtLabel3, gtLabel4, gtLabel5, gtLabel6,
                                                gtLabel7, gtLabel8, gtLabel9, gtLabel10};

                int highScoreListsCount = Scores.getHighScoreListCount();
                int i; ;

                for (i = 0; i < highScoreListsCount; i++)
                {
                    correctAnswersLabelArray[i].Content = Scores.getNumCorrectAnswers(i);
                }

                for (i = 0; i < highScoreListsCount; i++)
                {
                    inCorrectAnswersLabelArray[i].Content = Scores.getNumIncorrectAnswers(i);
                }

                for (i = 0; i < highScoreListsCount; i++)
                {
                    gameTimeLabelArray[i].Content = Scores.getGameTime(i);
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        
        private void scoreFeedBack()
        {
            try
            {
                int correct = Scores.score.numCorrectAnswers;
                int incorrect = Scores.score.numIncorrectAnswers;

                if (correct == 10)
                {
                    Sounds.perfectScore();
                    scoreFeedBackLabel.Content = "perfect score!!\nyou must be a jedi master";
                }
                else if (correct >= 8)
                {
                    Sounds.greatScore();
                    scoreFeedBackLabel.Content = "impressive!\nyou must be a jedi knight";
                }
                else if (correct >= 6)
                {
                    Sounds.averageScore();
                    scoreFeedBackLabel.Content = "not bad. with a little more practice \nyou will soon become a jedi knight";
                }
                else if (correct >= 4)
                {
                    Sounds.belowAverage();
                    scoreFeedBackLabel.Content = "keep practicing jedi initiate.\nyou still have much to learn.";
                }
                else
                {
                    Sounds.fail();
                    scoreFeedBackLabel.Content = "oh no, i fear that you may not possess the force.\n" +
        "                                       keep practicing!";
                }
                if (Scores.isInHighScoreLists)
                {
                    didMakeHighScoreListsLabel.Content = "you made it on the high score list";
                }
                else
                {
                    didMakeHighScoreListsLabel.Content = "you didn't make it on the high score list.\nkeep trying!";
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
