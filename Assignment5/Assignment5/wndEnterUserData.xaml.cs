﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Assignment5
{
    /// <summary>
    /// Interaction logic for wndEnterUserData.xaml
    /// </summary>
    public partial class wndEnterUserData : Window
    {
        bool submittedUserData;
        public wndEnterUserData()
        {
            InitializeComponent();
            
            submittedUserData = false;
            userNameTextBox.Focus();
        }
        /// <summary>
        /// processes events that need to occur when the user clicks the done button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void doneUserDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                done();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes validation for the age text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userAgeTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ageValidation(sender, e);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// processes validation for the name text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userNameTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                nameValidation(sender, e);
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        /// <summary>
        /// logic to validate user name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nameValidation(object sender, KeyEventArgs e)
        {
            try
            {
                if (!(e.Key >= Key.A && e.Key <= Key.Z))
                {
                    //Allow the user to use the backspace, delete, enter, space, and tab
                    if (!(e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.Tab))
                    {
                        //No other keys allowed besides numbers, backspace, enter/return, and delete
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// logic for when the user clicks the done button
        /// </summary>
        private void done()
        {
            try
            {
                if (string.IsNullOrEmpty(userNameTextBox.Text))
                    enterName();

                else if (string.IsNullOrEmpty(userAgeTextBox.Text))
                    enterAge();

                else
                {
                    submittedUserData = true;
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// logic to validate user age
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ageValidation(object sender, KeyEventArgs e)
        {
            try
            {
                if (!((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
                {
                    //Allow the user to use the backspace and delete keys
                    if (!(e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Enter))
                    {
                        //No other keys allowed besides numbers, backspace, enter/return, and delete
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays message box if user has not entered their name
        /// </summary>
        private void enterName()
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("UH OH! You didn't enter your name.", "Enter Name");
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// displays message box if user has not entered their age
        /// </summary>
        private void enterAge()
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("UH OH! You didn't enter your age.", "Enter Age");
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception               
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// logic to determine if the user submitted a valid name and age
        /// </summary>
        public bool DidSubmit
        {
            get
            {
                return submittedUserData;
            }
        }
        /// <summary>
        /// returns user name
        /// </summary>
        public string GetName
        {
            get
            {
                return userNameTextBox.Text;
            }
        }
        /// <summary>
        /// returns user age
        /// </summary>
        public string GetAge
        {
            get
            {
                return userAgeTextBox.Text;
            }
        }
        /// <summary>
        /// displays errors passed from other methods to a window for debugging purposes
        /// </summary>
        /// <param name="sClass"></param>
        /// <param name="sMethod"></param>
        /// <param name="sMessage"></param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                //Would write to a file or database here.
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
    }
}
