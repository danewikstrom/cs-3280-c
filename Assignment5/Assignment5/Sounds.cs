﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Reflection;

namespace Assignment5
{
    /// <summary>
    /// Class that is used to process all sounds that are used in the game
    /// </summary>
    public static class Sounds
    {
        /// <summary>
        /// Called when user answer a question correct. Plays appropriate .wav file.
        /// </summary>
        public static void getCorrectSound()
        {
            try
            {
                SoundPlayer R2D2woohoo = new SoundPlayer("R2D2woohoo.wav");
                R2D2woohoo.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when user answer a question incorrect. Plays appropriate .wav file.
        /// </summary>
        public static void getIncorrectSound()
        {
            try
            {
                SoundPlayer chewyRoar = new SoundPlayer("chewy_roar.wav");
                chewyRoar.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when user doesn't enter an answer into the answer text box. Plays appropriate .wav file.
        /// </summary>
        public static void dontDoThat()
        {
            try
            {
                SoundPlayer dontDoThat = new SoundPlayer("don'tDoThat.wav");
                dontDoThat.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the add game is started. Plays appropriate .wav file.
        /// </summary>
        public static void startAddGame()
        {
            try
            {
                SoundPlayer startWav = new SoundPlayer("do_or_do_not.wav");
                startWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the subtract game is started. Plays appropriate .wav file.
        /// </summary>
        public static void startSubtractGame()
        {
            try
            {
                SoundPlayer startWav = new SoundPlayer("dont_underestimate.wav");
                startWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the multiplication game is started. Plays appropriate .wav file.
        /// </summary>
        public static void startMultiplyGame()
        {
            try
            {
                SoundPlayer startWav = new SoundPlayer("light-saber-on.wav");
                startWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the division game is started. Plays appropriate .wav file.
        /// </summary>
        public static void startDivideGame()
        {
            try
            {
                SoundPlayer startWav = new SoundPlayer("theForceWillAlwaysBeWithYou.wav");
                startWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the user clicks the end game button. Plays appropriate .wav file.
        /// </summary>
        public static void endGame()
        {
            try
            {
                SoundPlayer endWav = new SoundPlayer("light-saber-off.wav");
                endWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the user gets a perfect score. Plays appropriate .wav file.
        /// </summary>
        public static void perfectScore()
        {
            try
            {
                SoundPlayer strongWav = new SoundPlayer("forceisstrong.wav");
                strongWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the user gets a great score. Plays appropriate .wav file.
        /// </summary>
        public static void greatScore()
        {
            try
            {
                SoundPlayer taughtWellWav = new SoundPlayer("taught_u_well.wav");
                taughtWellWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the user gets an average score. Plays appropriate .wav file.
        /// </summary>
        public static void averageScore()
        {
            try
            {
                SoundPlayer forceWav = new SoundPlayer("theForceWillAlwaysBeWithYou.wav");
                forceWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the user gets a below average score. Plays appropriate .wav file.
        /// </summary>
        public static void belowAverage()
        {
            try
            {
                SoundPlayer disturbenceWav = new SoundPlayer("disturbence.wav");
                disturbenceWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the user gets a failing score. Plays appropriate .wav file.
        /// </summary>
        public static void fail()
        {
            try
            {
                SoundPlayer darthVaderBreathingWav = new SoundPlayer("darthVaderBreathing.wav");
                darthVaderBreathingWav.Play();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
    }
}


