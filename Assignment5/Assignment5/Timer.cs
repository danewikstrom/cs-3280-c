﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Threading;
using System.Diagnostics;
//http://www.c-sharpcorner.com/blogs/creating-a-simple-stop-watch-application-in-wpf1 for stopwatch

namespace Assignment5
{
    /// <summary>
    /// Class to used to show elapsed time on game screen and for the high scores list
    /// </summary>
    public class Timer
    {

        public DispatcherTimer myTimer;
        public Stopwatch stopWatch;
        public TimeSpan timeSpan;
        public string elapsedTime;

        public Timer()
        {
            myTimer = new DispatcherTimer();
            myTimer.Interval = TimeSpan.FromSeconds(1);
            myTimer.Tick += MyTimer_Tick;
            stopWatch = new Stopwatch();
            timeSpan = new TimeSpan();
            elapsedTime = string.Empty;
        }

        /// <summary>
        /// Updates timeSpan and elsapsedTime variables at ever tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MyTimer_Tick(object sender, EventArgs e)
        {
            if (stopWatch.IsRunning)
            {
                timeSpan = stopWatch.Elapsed;
                elapsedTime = String.Format("minute: {0:0}, second: {1:0}",
                timeSpan.Minutes, timeSpan.Seconds);
            }
        }

        /// <summary>
        /// Starts myTime and stopWatch variables
        /// </summary>
        public void startTimer()
        {
            myTimer.Start();
            stopWatch.Start();
        }

        /// <summary>
        /// Stops myTime and stopWatch variables
        /// </summary>
        public void stopTimer()
        {
            myTimer.Stop();
            stopWatch.Stop();
        }

        /// <summary>
        /// Resets the timer
        /// </summary>
        public void resetTimer()
        {
            stopWatch.Reset();
        }
    }
}
