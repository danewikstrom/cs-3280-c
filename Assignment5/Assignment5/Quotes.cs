﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;

namespace Assignment5
{
    /// <summary>
    /// Class used to get quotes to display when playing the game
    /// </summary>
    public static class Quotes
    {
        /// <summary>
        /// randomly selects a quote to be displayed on the screen
        /// </summary>
        /// <returns></returns>
        public static string getRandomQuote()
        {
            try
            {
                Random randomQuote = new Random();
                int randNum = randomQuote.Next(0, 12);

                if (randNum == 0)
                    return "Your focus determines your reality.\n- Que-Gon Jinn";
                else if (randNum == 1)
                    return "Do. Or do not. There is no try.\n– Yoda";
                else if (randNum == 2)
                    return "In my experience there is no such thing as luck.\n– Obi-Wan Kenobi";
                else if (randNum == 3)
                    return "Remember…the Force will be with you, always.\n– Obi Wan Kenobi";
                else if (randNum == 4)
                    return "Stay on target.\n– Gold Five";
                else if (randNum == 5)
                    return "Fear is the path to the Dark Side. Fear leads to anger,\nanger leads to hate, hate leads to suffering.\n- Yoda";
                else if (randNum == 6)
                    return "No! Try not. Do, or do not. There is no try.\n- Yoda";
                else if (randNum == 7)
                    return "Size matters not. Look at me. \nJudge me by my size, do you?\n- Yoda";
                else if (randNum == 8)
                    return "The fear of loss is a path to the Dark Side.\n- Yoda";
                else if (randNum == 9)
                    return "Always pass on what you have learned.\n- Yoda";
                else
                    return "Truly wonderful the mind of a child is.\n- Yoda";
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
    }
}
