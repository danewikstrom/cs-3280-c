﻿namespace Assignment5
{
    /// <summary>
    /// abstract class that add, subtract, multiply, and divide classes inherit from
    /// </summary>
    abstract class Game
    {
        /// <summary>
        /// all games use a timer to record the game's elapsed time
        /// </summary>
        public Timer timer = new Timer();
        /// <summary>
        /// records what type of game is being played
        /// </summary>
        public gameType gameChoice;
        /// <summary>
        /// holds the number of correct answers for a game
        /// </summary>
        public int numCorrectAnswers;
        /// <summary>
        /// holds the number of incorrect answers for a game
        /// </summary>
        public int numInCorrectAnswers;

        public enum gameType
        {
            add,
            subtract,
            multiply,
            divide
        }
        /// <summary>
        /// Randomly generate left and right hand operands
        /// </summary>
        /// <returns>the question</returns>
        public abstract string createRandomQuestion();
        /// <summary>
        /// Determines if answer is correct and increments the number of (in)correct answers
        /// </summary>
        /// <param name="userAnswer"></param>
        /// <returns>true or false if answer is correct</returns>
        public abstract bool isAnswerCorrect(int userAnswer);

    }
}
